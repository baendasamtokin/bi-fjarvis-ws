package is.bondi.bifjarvisws;

import java.io.Serializable;

public class Fang implements Serializable{

    private int fangNumer;
    private int modirNumer;
    private int fadirNumer;
    private String dagsetning;
    private String fangAr;

    public Fang() {
    }
    
    public Fang(int fangNumer, int modirNumer, int fadirNumer, String dagsetning, String fangAr ) {
        this.fangNumer = fangNumer;
        this.modirNumer = modirNumer;
        this.fadirNumer = fadirNumer;
        this.dagsetning = dagsetning;
        this.fangAr = fangAr;
    }


    public void setFangNumer(int fangNumer) {
        this.fangNumer = fangNumer;
    }

    public int getFangNumer() {
        return fangNumer;
    }

    public void setModirNumer(int modirNumer) {
        this.modirNumer = modirNumer;
    }

    public int getModirNumer() {
        return modirNumer;
    }
    
    public void setFadirNumer(int fadirNumer) {
        this.fadirNumer = fadirNumer;
    }
    
    public int getFadirNumer() {
        return fadirNumer;
    }

    public void setDagsetning(String dagsetning) {
        this.dagsetning = dagsetning;
    }

    public String getDagsetning() {
        return dagsetning;
    }
    
    public void setFangAr(String fangAr) {
        this.fangAr = fangAr;
    }
    
    public String getFangAr() {
        return fangAr;
    }
}
