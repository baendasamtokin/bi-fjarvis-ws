package is.bondi.bifjarvisws;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import java.net.InetAddress;

import java.net.UnknownHostException;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.sql.Types;

import javax.jws.WebMethod;
import javax.jws.WebService;

import javax.naming.NamingException;

import oracle.webservices.annotations.Deployment;


@WebService(serviceName = "BiFjarvisWs")
@Deployment(restSupport = true)

public class BiFjarvisWs 
{
  public BiFjarvisWs() 
  {
  }

  private String getDatabaseName()
  {
    return "jdbc/LambDS"; 
  }

  @WebMethod
  public BylisHrutur[] getFarmRamData(int userID, String clientID, int systemID, int farmID) throws AuthenticationException, DatabaseException, DataNotFoundException, UnknownException
  {   
    Connection conn = null;
    ResultSet rsGogn = null;
    PreparedStatement pstmt = null;
    BylisHrutur[] gogn = null;
    DatabaseConnection databaseConnection = new DatabaseConnection(getDatabaseName());
      
    try
    {
      try 
      {
        conn = databaseConnection.getConnection();
      } 
      catch (SQLException e) 
      {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        e.printStackTrace(new PrintStream(os));
        throw new DatabaseException(e.getClass().getName(), os.toString(), "Error while trying to get SQL Connection from DataSource.");
      }
      catch (NamingException e) 
      {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        e.printStackTrace(new PrintStream(os));
        throw new DatabaseException(e.getClass().getName(), os.toString(), "Error while trying to locate DataSource.");
      }
      
      //Au�kenning
      try
      {
        if(!getAuthentication(conn, userID, clientID, systemID))
        {
          throw new AuthenticationException("Username, password and system tuple is invalid.");
        }
      }
      catch(SQLException e)
      {
        e.printStackTrace();
      }

      //skrifa logg
      //IP hva�a - athuga seinna
      writeLog(conn, "getFarmRamData", clientID, null, null, null);

      //S�kja g�gn
                
      //RamDataOut[] gogn = null;
      int nFjoldi = 0;
          
      String sSQL;
          
      try 
      {     
        sSQL = "select busnumer, valnumer, ormerki, hrutur_numer, nafn, litur, " +
               "blup_fita, blup_gerd, blup1_mjolkurlagni, blup1_frjosemi, " +
                "blup2_mjolkurlagni, blup2_frjosemi, blup3_mjolkurlagni, " +
                "blup3_frjosemi, blup4_mjolkurlagni, blup4_frjosemi, " +
                "blup_einkunn_m, blup_einkunn_f, " +
                "valnumer_ff, ormerki_ff, numer_ff, nafn_ff, litur_ff, " +
                "valnumer_fff, ormerki_fff, numer_fff, nafn_fff, litur_fff, " +
                "valnumer_mm, ormerki_mm, numer_mm, nafn_mm, litur_mm, " +
                "valnumer_mmm, ormerki_mmm, numer_mmm, nafn_mmm, litur_mmm " +
                "from v_ofeigur_hrutur " +
                //"where busnumer = " + farmID +
                "where busnumer = ? " + //��
                "order by valnumer";

        pstmt = conn.prepareStatement(sSQL, java.sql.ResultSet.TYPE_SCROLL_SENSITIVE, java.sql.ResultSet.CONCUR_READ_ONLY);
        pstmt.setInt(1, farmID); //���
              
        rsGogn = pstmt.executeQuery();
          
        //fj�ldi f�rslna
        rsGogn.last();
        nFjoldi = rsGogn.getRow();
        rsGogn.beforeFirst();  
      } 
      catch (SQLException e) 
      { 
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        e.printStackTrace(new PrintStream(os));
        throw new DatabaseException(e.getClass().getName(), os.toString(), "Error while preparing search statement.");
      }

      try 
      {  
        if(nFjoldi == 0)
          throw new DataNotFoundException("Could not find any data based on your search input.");
          
        int nLoop = 0;
        gogn = new BylisHrutur[nFjoldi];
          
        while(rsGogn.next() && nLoop <= nFjoldi)
        {           
          BylisHrutur Hrutur = new BylisHrutur(
            rsGogn.getInt("busnumer"), 
            rsGogn.getString("valnumer"), 
            rsGogn.getString("ormerki"), 
            rsGogn.getInt("hrutur_numer"), 
            rsGogn.getString("nafn"), 
            rsGogn.getInt("litur"),
            rsGogn.getInt("blup_fita"), 
            rsGogn.getInt("blup_gerd"),
            rsGogn.getInt("blup1_mjolkurlagni"),
            rsGogn.getInt("blup1_frjosemi"),
            rsGogn.getInt("blup2_mjolkurlagni"),
            rsGogn.getInt("blup2_frjosemi"),
            rsGogn.getInt("blup3_mjolkurlagni"),
            rsGogn.getInt("blup3_frjosemi"),
            rsGogn.getInt("blup4_mjolkurlagni"),
            rsGogn.getInt("blup4_frjosemi"),
            rsGogn.getInt("blup_einkunn_m"),
            rsGogn.getInt("blup_einkunn_f"),
            rsGogn.getString("valnumer_ff"),
            rsGogn.getString("ormerki_ff"),
            rsGogn.getInt("numer_ff"),
            rsGogn.getString("nafn_ff"),
            rsGogn.getInt("litur_ff"),
            rsGogn.getString("valnumer_fff"),
            rsGogn.getString("ormerki_fff"),
            rsGogn.getInt("numer_fff"),
            rsGogn.getString("nafn_fff"),
            rsGogn.getInt("litur_fff"),
            rsGogn.getString("valnumer_mm"),
            rsGogn.getString("ormerki_mm"),
            rsGogn.getInt("numer_mm"),
            rsGogn.getString("nafn_mm"),
            rsGogn.getInt("litur_mm"),
            rsGogn.getString("valnumer_mmm"),
            rsGogn.getString("ormerki_mmm"),
            rsGogn.getInt("numer_mmm"),
            rsGogn.getString("nafn_mmm"),
            rsGogn.getInt("litur_mmm")
          );
                
          gogn[nLoop] = Hrutur;  
          nLoop++;
        }
      }
      catch (SQLException e) 
      {
        throw new DataNotFoundException("Could not find any data based on your search input.");
      }
      
      try 
      {
        databaseConnection.close();
      } 
      catch (SQLException e) 
      {
        throw new DataNotFoundException("Could not close databaseconnection: " + e.getMessage());
      }      
    }
    finally
    {
      gangaFraFinally(rsGogn, pstmt, conn);
    }
    return gogn;
  }
  
    
  //----------------------------------------------------------------------------    
          
  /* pushSpringData()
   * inn: int userID, String clientID, int systemID, int farmID
   * �tt: fylki
   **/

  @WebMethod
  public RFIDIn pushSpringData(int userID, String clientID, int systemID, int farmID, VorAer aer) throws  AuthenticationException, DatabaseException, DataNotFoundException, UnknownException 
  {
    RFIDIn inn = new RFIDIn();
    
    Connection conn = null;
    CallableStatement cstmt = null;
    CallableStatement cstmtVilla = null;

    DatabaseConnection databaseConnection = new DatabaseConnection(getDatabaseName());
   
    try
    {
      try 
      {
        conn = databaseConnection.getConnection();
      } 
      catch (SQLException e) 
      {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        e.printStackTrace(new PrintStream(os));
        throw new DatabaseException(e.getClass().getName(), os.toString(), "Error while trying to get SQL Connection from DataSource.");
      }
      catch (NamingException e) 
      {
          ByteArrayOutputStream os = new ByteArrayOutputStream();
          e.printStackTrace(new PrintStream(os));
          throw new DatabaseException(e.getClass().getName(), os.toString(), "Error while trying to locate DataSource.");
      }
        
      //au�kenning
      try
      {
        if (!getAuthentication(conn, userID, clientID, systemID)) 
        {
          throw new AuthenticationException("Username, password and system tuple is invalid.");
        }
      }
      catch(SQLException e)
      {
        e.printStackTrace();
      }
      
      //skrifa logg
      writeLog(conn, "pushSpringData", clientID, null, null, null);
        

      try {
        conn.setAutoCommit(false);
      }
      catch (SQLException e) 
      {
        inn.setWarning("Could not set connection to autocommit: " + e.getMessage());
      }

      int nVilla;
      int nFangNumer;
      int fjoldiIBurdi = aer.getFjoldiIBurdi();
      int fjoldiLamba = aer.getLamb().length;
      
      try 
      {          
        // Ef veri� er a� kalla � function en ekki procedure m� nota: { call ? := setInngogn(?,?,?,?,?) }
        // spOrmerki(
            //1) p_gripur_numer IN NUMBER, 
            //2) p_ormerki IN VARCHAR2, 
            //3) p_framleidsluartal IN NUMBER,
            //4) p_skrad_af IN NUMBER, 
            //5) p_villa OUT NUMBER)

      
        // Lesa fyrst inn m��urina
        cstmt = conn.prepareCall("{ call pk_ofeigur.modir_vor(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
        
        cstmt.setInt(1, userID);
        cstmt.setInt(2, farmID);
        cstmt.setInt(3, aer.getAerNumer());
        cstmt.setString(4, aer.getOrmerki());
        cstmt.setString(5, aer.getNafn());
        cstmt.setInt(6, aer.getBeitarsvaediNumer());
        cstmt.setInt(7, aer.getHruturNumer());
        cstmt.setString(8, aer.getFangDags());
        cstmt.setInt(9, aer.getAfdrifFangs());
        cstmt.setInt(10, aer.getFjoldiIBurdi());
        cstmt.setString(11, aer.getBurdarDags());
        cstmt.setString(12, aer.getBIJS());
        
        cstmt.registerOutParameter(13, Types.INTEGER);
        cstmt.registerOutParameter(14, Types.INTEGER);
        
        cstmt.execute();

        nFangNumer = cstmt.getInt(13);
        nVilla = cstmt.getInt(14);

        //System.out.println(nVilla);
        if(nVilla == 0)
        {
          for (int idx=0; idx < aer.getLamb().length; idx++) 
          {
            if (nVilla == 0)
            {
              cstmt.close();
              cstmt = null;            
              cstmt = conn.prepareCall("{call pk_ofeigur.lamb_vor(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                
              VorLamb tmpLamb = aer.getLamb()[idx];
              cstmt.setInt(1, userID );
              cstmt.setInt(2, farmID );
              cstmt.setInt(3, aer.getAerNumer());
              cstmt.setInt(4, nFangNumer);
              cstmt.setString(5, aer.getBurdarDags());                        
              cstmt.setString(6, tmpLamb.getLambanumer());
              cstmt.setString(7, tmpLamb.getOrmerki());
              cstmt.setInt(8, tmpLamb.getKyn());
              cstmt.setInt(9, tmpLamb.getLitanumer());
              cstmt.setString(10, tmpLamb.getFaedingarThungi());
              cstmt.setInt(11, tmpLamb.getFjoldiGekk());
              cstmt.setInt(12, tmpLamb.getFosturmodirNumer());
              cstmt.setInt(13, tmpLamb.getAfdrifVor());
              cstmt.registerOutParameter(14, Types.INTEGER);
              cstmt.execute();
      
              nVilla = cstmt.getInt(14);
            }
          }
        }
        
        //System.out.println(nVilla);
        

        if(nVilla != 0)
        {
          //me�h�ndlun villu, sett inn 04.10.2010, IAR.
          String sVilla = "";
          cstmtVilla = conn.prepareCall("{ call ? := fn_get_villa(?)}");
          cstmtVilla.registerOutParameter(1, Types.VARCHAR);
          cstmtVilla.setInt(2, nVilla);
          cstmtVilla.execute();
          sVilla = cstmtVilla.getString(1);
          cstmtVilla.close();
          //System.out.println(nVilla);
          inn.setWarning(sVilla);
        }

        inn.setResult(nVilla);
        
        //System.out.println(nVilla);            
        if(nVilla == 0)
          conn.commit();
        else
          conn.rollback();
      } 
      catch (SQLException e) 
      {
          ByteArrayOutputStream os = new ByteArrayOutputStream();
          e.printStackTrace(new PrintStream(os));
          throw new DatabaseException(e.getClass().getName(), os.toString(), "Error while preparing update call.");
      }   

      try 
      {
        databaseConnection.close();
      } 
      catch (SQLException e) 
      {
        inn.setWarning("Could not close databaseconnection: " + e.getMessage());
      } 
    }
    finally
    {
      gangaFraFinally(cstmt, conn);
      gangaFraFinally(cstmtVilla, conn);
    }

    return inn;
  }
   
  /* getSpringData() - s�kir allar �r og l�mb �eirra
  * inn: int userID, String clientID, int systemID, int farmID
  * �t: fylki me� �num og l�mbum �eirra
  **/
    
  @WebMethod
  public SpringDataOut[] getSpringData(int userID, String clientID, int systemID, int farmID) throws AuthenticationException, DatabaseException, DataNotFoundException, UnknownException
  {   
    Connection conn = null;
    ResultSet rsGogn = null;
    PreparedStatement pstmt = null;
    SpringDataOut[] gogn = null;
    
    DatabaseConnection databaseConnection = new DatabaseConnection(getDatabaseName());
    
    try
    {
      try 
      {
        conn = databaseConnection.getConnection();
      } 
      catch (SQLException e) 
      {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        e.printStackTrace(new PrintStream(os));
        throw new DatabaseException(e.getClass().getName(), os.toString(), "Error while trying to get SQL Connection from DataSource.");
      }
      catch (NamingException e) 
      {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        e.printStackTrace(new PrintStream(os));
        throw new DatabaseException(e.getClass().getName(), os.toString(), "Error while trying to locate DataSource.");
      }
      
      //Au�kenning
      try
      {    
        if(!getAuthentication(conn, userID, clientID, systemID))
        {
          throw new AuthenticationException("Username, password and system tuple is invalid.");
        }
      }
      catch(SQLException e)
      {
        e.printStackTrace();
      }

      //skrifa logg
      writeLog(conn, "getSpringData", clientID, null, null, null);

      //S�kja g�gn
      
      int nFjoldi = 0;
      
      String sSQL;
      try 
      {     
        sSQL =  "select MODIR_VALNR, MODIR_ORMERKI, MODIR_NUMER, " + 
                "MODIR_NAFN, MODIR_BEITARSVAEDI_NUMER, FJOLDI_LAMBA, BURDARDAGS, FADIR_VALNUMER as fadir_valnr, " + 
                "FADIR_ORMERKI, FADIR_NUMER, FADIR_NAFN, FANGDAGS, AFDRIF_FANGS_NUMER, " + 
                "MODIR_ATHS_NUMER, LAMBANUMER, ORMERKI, LAMB_NUMER, KYN, LITUR_NUMER, " + 
                "FOSTURMODIR, FOSTURMODIR_NUMER, FOSTURMODIR_ORMERKI, FAEDINGARTHUNGI, AFDRIF_VOR_NUMER, AFDRIF_VOR_FASTI " +
                "from v_ofeigur_vorgogn " + 
                //"where byli_numer = " + farmID +
                "where byli_numer = ? " + //���
                "order by modir_valnr ";//mj�g mikilv�gt a� �etta s� inni, m� ekki breyta (Hj�lmar notar �essa r��un)!!!

        pstmt = conn.prepareStatement(sSQL, java.sql.ResultSet.TYPE_SCROLL_SENSITIVE, java.sql.ResultSet.CONCUR_READ_ONLY);
        pstmt.setInt(1, farmID); //���
        rsGogn = pstmt.executeQuery();
          
        //fj�ldi f�rslna
        rsGogn.last();
        nFjoldi = rsGogn.getRow();
        rsGogn.beforeFirst();
      } 
      catch (SQLException e) 
      {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        e.printStackTrace(new PrintStream(os));
        throw new DatabaseException(e.getClass().getName(), os.toString(), "Error while preparing search statement.");
      }

      try 
      {  
        if(nFjoldi == 0)
          throw new DataNotFoundException("Could not find any data based on your search input.");
          
        int nLoop = 0;
        gogn = new SpringDataOut[nFjoldi];
        
        String sModirValnr = "";
        String sModirOrmerki = "";
        String sModirNumer = "";
        String sModirNafn = "";
        String sModirBeitarsvaediNr = "";
        String sFjoldiLamba = "";
        String sBurdardags = "";
        String sFadirValnr = "";
        String sFadirOrmerki = "";
        String sFadirNumer = "";
        String sFadirNafn = "";
        String sFangdags = "";
        String sAfdrifFangsNumer = "";
        String sModirAthsNumer = "";
        String sLambanumer = "";
        String sOrmerki = "";
        String sLambNumer = "";
        String sKyn = "";
        String sLitanumer = "";
        String sFosturmodir = "";
        String sFosturmodirNumer = "";
        String sFosturmodirOrmerki = "";
        String sFaedingarthungi = "";
        String sAfdrifVorNumer = "";
        String sAfdrifVorFasti = "";
        
        while(rsGogn.next() && nLoop <= nFjoldi)
        {   
          if(rsGogn.getString("modir_valnr") != null) sModirValnr = rsGogn.getString("modir_valnr");  
          else sModirValnr = "";
          if(rsGogn.getString("modir_ormerki") != null) sModirOrmerki = rsGogn.getString("modir_ormerki");  
          else sModirOrmerki = "";
          if(rsGogn.getString("modir_numer") != null) sModirNumer = rsGogn.getString("modir_numer");  
          else sModirNumer = "";
          if(rsGogn.getString("modir_nafn") != null) sModirNafn = rsGogn.getString("modir_nafn");  
          else sModirNafn = "";
          if(rsGogn.getString("modir_beitarsvaedi_numer") != null) sModirBeitarsvaediNr = rsGogn.getString("modir_beitarsvaedi_numer");  
          else sModirBeitarsvaediNr = "";                    
          if(rsGogn.getString("fjoldi_lamba") != null) sFjoldiLamba = rsGogn.getString("fjoldi_lamba");  
          else sFjoldiLamba = "";
          if(rsGogn.getString("burdardags") != null) sBurdardags = rsGogn.getString("burdardags");  
          else sBurdardags = "";
          if(rsGogn.getString("fadir_valnr") != null) sFadirValnr = rsGogn.getString("fadir_valnr");  
          else sFadirValnr = "";
          if(rsGogn.getString("fadir_ormerki") != null) sFadirOrmerki = rsGogn.getString("fadir_ormerki");  
          else sFadirOrmerki = "";
          if(rsGogn.getString("fadir_numer") != null) sFadirNumer = rsGogn.getString("fadir_numer");  
          else sFadirNumer = "";
          if(rsGogn.getString("fadir_nafn") != null) sFadirNafn = rsGogn.getString("fadir_nafn");  
          else sFadirNafn = "";
          if(rsGogn.getString("fangdags") != null) sFangdags = rsGogn.getString("fangdags");  
          else sFangdags = "";
          if(rsGogn.getString("afdrif_fangs_numer") != null) sAfdrifFangsNumer = rsGogn.getString("afdrif_fangs_numer");  
          else sAfdrifFangsNumer = "";
          if(rsGogn.getString("modir_aths_numer") != null) sModirAthsNumer = rsGogn.getString("modir_aths_numer");  
          else sModirAthsNumer = "";
          if(rsGogn.getString("lambanumer") != null) sLambanumer = rsGogn.getString("lambanumer");  
          else sLambanumer = "";
          if(rsGogn.getString("ormerki") != null) sOrmerki = rsGogn.getString("ormerki");  
          else sOrmerki = "";
          if(rsGogn.getString("lamb_numer") != null) sLambNumer = rsGogn.getString("lamb_numer");  
          else sLambNumer = "";
          if(rsGogn.getString("kyn") != null) sKyn = rsGogn.getString("kyn");  
          else sKyn = "";
          if(rsGogn.getString("litur_numer") != null) sLitanumer = rsGogn.getString("litur_numer");  
          else sLitanumer = "";
          if(rsGogn.getString("fosturmodir") != null) sFosturmodir = rsGogn.getString("fosturmodir");  
          else sFosturmodir = "";
          if(rsGogn.getString("fosturmodir_numer") != null) sFosturmodirNumer = rsGogn.getString("fosturmodir_numer");  
          else sFosturmodirNumer = "";
          if(rsGogn.getString("fosturmodir_ormerki") != null) sFosturmodirOrmerki = rsGogn.getString("fosturmodir_ormerki");  
          else sFosturmodirOrmerki = "";
          if(rsGogn.getString("faedingarthungi") != null) sFaedingarthungi = rsGogn.getString("faedingarthungi");  
          else sFaedingarthungi = "";
          if(rsGogn.getString("afdrif_vor_numer") != null) sAfdrifVorNumer = rsGogn.getString("afdrif_vor_numer");  
          else sAfdrifVorNumer = "";
          if(rsGogn.getString("afdrif_vor_fasti") != null) sAfdrifVorFasti = rsGogn.getString("afdrif_vor_fasti");  
          else sAfdrifVorFasti = "";
    
          //System.out.println(rsGogn.getString("fadir_ormerki"));
          SpringData springData = new SpringData(
          sModirValnr, 
          sModirOrmerki, 
          sModirNumer,
          sModirNafn,
          sModirBeitarsvaediNr,
          sFjoldiLamba,
          sBurdardags,
          sFadirValnr,
          sFadirOrmerki,
          sFadirNumer,
          sFadirNafn,
          sFangdags,
          sAfdrifFangsNumer,
          sModirAthsNumer,
          sLambanumer,
          sOrmerki,
          sLambNumer,
          sKyn,
          sLitanumer,
          sFosturmodir,
          sFosturmodirNumer,
          sFosturmodirOrmerki,
          sFaedingarthungi,
          sAfdrifVorNumer,
          sAfdrifVorFasti);
          
          gogn[nLoop] = new SpringDataOut(springData);
                  
          nLoop++;
        }
      }
      catch (SQLException e) 
      {
        throw new DataNotFoundException("Could not find any data based on your search input.");
      }

      try 
      {
        databaseConnection.close();
      } 
      catch (SQLException e) 
      {
        throw new DataNotFoundException("Could not close databaseconnection: " + e.getMessage());
      }   
    }
    finally
    {
      gangaFraFinally(rsGogn, pstmt, conn);               
    }
     
    return gogn;
  }

    /*S�kja gripi
     * */
          

    @WebMethod
    public RFIDOut[] getSheepRearwards(int userID, String clientID, int systemID, int farmID) 
                                                throws 
                                                    AuthenticationException,
                                                    DatabaseException, 
                                                    DataNotFoundException, 
                                                    UnknownException 
    {   
        Connection conn = null;
        RFIDOut[] gogn = null;
        ResultSet rsGogn = null;
        PreparedStatement pstmt = null;
        
        DatabaseConnection databaseConnection = 
        /*Qurinus*///new DatabaseConnection("jdbc/bondiDS");
        /*Pan*///new DatabaseConnection("jdbc/S2000_PanDS");
        new DatabaseConnection(getDatabaseName());
        
        try
        {
            try 
            {
                conn = databaseConnection.getConnection();
            } 
            catch (SQLException e) 
            {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                e.printStackTrace(new PrintStream(os));
                throw new DatabaseException(e.getClass().getName(), 
                                            os.toString(), 
                                            "Error while trying to get SQL Connection from DataSource.");
            }
            catch (NamingException e) 
            {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                e.printStackTrace(new PrintStream(os));
                throw new DatabaseException(e.getClass().getName(), 
                                            os.toString(), 
                                            "Error while trying to locate DataSource.");
            }
            //Au�kenning
            try
            {
                
                if(!getAuthentication(conn, userID, clientID, systemID))
                {
                    throw new AuthenticationException("Username, password and system tuple is invalid.");
                }
            }
            catch(SQLException e)
            {
                e.printStackTrace();
            }
    
            //skrifa logg
            writeLog(conn, "getSheepRearwards", clientID, null, null, null);
    
            //S�kja g�gn
            //RFIDOut[] gogn = null;
            int nFjoldi = 0;
            String sSQL;
            try 
            {     
                sSQL =  "select byli_numer as busnumer, einstaklingur_numer as gripur_numer, " +
                        "einstaklingur_nafn as nafn, litur, " +
                        "fadir, modir, fadir_nafn, modir_nafn, ormerki as rfid, merki as vid " + 
                        "from v_vasa_ormerki " + 
                        //"where byli_numer = " + farmID + " " +
                        "where byli_numer = ? " +
                        "order by merki";
    
                pstmt = conn.prepareStatement(
                    sSQL,
                    java.sql.ResultSet.TYPE_SCROLL_SENSITIVE, 
                    java.sql.ResultSet.CONCUR_READ_ONLY);
                    
                pstmt.setInt(1, farmID);
                    
                rsGogn = pstmt.executeQuery();
                
                //fj�lda f�rslna
                rsGogn.last();
                nFjoldi = rsGogn.getRow();
                rsGogn.beforeFirst();
                
            } 
            catch (SQLException e) 
            {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                e.printStackTrace(new PrintStream(os));
                throw new DatabaseException(e.getClass().getName(), 
                                            os.toString(), 
                                            "Error while preparing search statement.");
            }
    
            try 
            {  
                if(nFjoldi == 0)
                    throw new DataNotFoundException("Could not find any data based on your search input.");
                
                int nLoop = 0;
                gogn = new RFIDOut[nFjoldi];
                
                while(rsGogn.next() && nLoop <= nFjoldi)
                {           
                   
                //public Sheep(int FVSeqNumber, int color,String fatherName, String mother, String father, String name)
                
                    Sheep sheepRearwards = new Sheep(
                        rsGogn.getInt("gripur_numer"), 
                        rsGogn.getInt("litur"), 
                        rsGogn.getString("vid"), 
                        rsGogn.getString("rfid"), 
                        rsGogn.getString("modir_nafn"), 
                        rsGogn.getString("fadir_nafn"),
                        rsGogn.getString("modir"), 
                        rsGogn.getString("fadir"),
                        rsGogn.getString("nafn")
                        );
                                    
                    gogn[nLoop] = new RFIDOut(
                        rsGogn.getInt("busnumer"),
                        sheepRearwards);
                        
                    nLoop++;
                }
            }
            catch (SQLException e) 
            {
                throw new DataNotFoundException("Could not find any data based on your search input.");
            }
    
            try 
            {
                //conn.close();
                databaseConnection.close();
            } 
            catch (SQLException e) 
            {
                throw new DataNotFoundException("Could not close databaseconnection: " + 
                                    e.getMessage());
                //gogn.setWarning("Could not close databaseconnection: " + 
                  //                e.getMessage());
    
            }
        }    
        finally
        {
            gangaFraFinally(rsGogn, pstmt, conn);               
        }

        return gogn;
    }
    //----------------------------------------------------------------------------    

    @WebMethod
    public RFIDIn pushAssembledRFIDofVID(int FVseqNumber, String rifID, int productionYear, 
                                         int userID, String clientID, int systemID) 
                                         throws AuthenticationException, 
                                                DatabaseException, 
                                                DataNotFoundException, 
                                                UnknownException 
    {

        RFIDIn inn = new RFIDIn();
        Connection conn = null;

        DatabaseConnection databaseConnection = 
        /*Qurinus*///new DatabaseConnection("jdbc/bondiDS");
        /*Pan*///new DatabaseConnection("jdbc/S2000_PanDS");
        new DatabaseConnection(getDatabaseName());
        CallableStatement cstmt = null;
        
        try
        {
            try 
            {
                conn = databaseConnection.getConnection();
            } 
            catch (SQLException e) 
            {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                e.printStackTrace(new PrintStream(os));
                throw new DatabaseException(e.getClass().getName(), 
                                            os.toString(), 
                                            "Error while trying to get SQL Connection from DataSource.");
            }
            catch (NamingException e) 
            {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                e.printStackTrace(new PrintStream(os));
                throw new DatabaseException(e.getClass().getName(), 
                                            os.toString(), 
                                            "Error while trying to locate DataSource.");
            }
            
            //au�kenning
            try
            {
                if (!getAuthentication(conn, userID, clientID, systemID)) 
                {
                    throw new AuthenticationException("Username, password and system tuple is invalid.");
                }
            }
            catch(SQLException e)
            {
                e.printStackTrace();
            }
            
            //skrifa logg
            writeLog(conn, "pushAssembledRFIDofVID", clientID, null, null, null);    
    
            int nVilla; 
            String sWarning;
            try 
            {
                // Ef veri� er a� kalla � function en ekki procedure m� nota: { call ? := setInngogn(?,?,?,?,?) }
                // spOrmerki(
                    //1) p_gripur_numer IN NUMBER, 
                    //2) p_ormerki IN VARCHAR2, 
                    //3) p_framleidsluartal IN NUMBER,
                    //4) p_skrad_af IN NUMBER, 
                    //5) p_villa OUT NUMBER)
                    
                cstmt = conn.prepareCall("{ call pk_ofeigur.sp_Ormerki(?,?,?,?,?,?)}");
        
                cstmt.setInt(1, FVseqNumber);
                cstmt.setString(2, rifID);
                cstmt.setInt(3, productionYear);
                cstmt.setInt(4, userID);
                cstmt.registerOutParameter(5, Types.INTEGER);
                cstmt.registerOutParameter(6, Types.VARCHAR);
                
                cstmt.execute();
                nVilla = cstmt.getInt(5);
                sWarning = cstmt.getString(6);
               // System.out.println(nVilla);
                
                inn.setResult(nVilla);
                inn.setWarning(sWarning);
                
                if(nVilla == 0)
                    conn.commit();
                else
                    conn.rollback();
            } 
            catch (SQLException e) 
            {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                e.printStackTrace(new PrintStream(os));
                throw new DatabaseException(e.getClass().getName(), 
                                            os.toString(), 
                                            "Error while preparing update call.");
            }   
            /*catch(DatabaseException ex){
                ex.getMessage();
            }*/
            try 
            {
                databaseConnection.close();
            } 
            catch (SQLException e) 
            {
                inn.setWarning("Could not close databaseconnection: " + 
                                       e.getMessage());
            }
            
        }
        finally
        {
            gangaFraFinally(cstmt, conn);
        }
        return inn;
    }
    
    /*
     * getUsers() - s�kir alla notendur �kve�ins aukennis fr� client
     * inn: client id ClID, Ipt�lu clients IP, Mac addressu clients MacAdd og �tg�fu clients ClVersion
     * �t: fylki me� �llum notendum + h�p + kerfi vi�komandi clients id
     **/

    //---------------------------------------------------------------

    @WebMethod
    public User[] getUsers(String ClID, String IP, String MacAdd, String  ClVersion) 
                                            throws  AuthenticationException, 
                                                    DatabaseException, 
                                                    DataNotFoundException, 
                                                    UnknownException 
    {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rsGogn = null;
        User[] users = null;
        
        DatabaseConnection databaseConnection = 
        /*Qurinus*///new DatabaseConnection("jdbc/bondiDS");
        /*Pan*///new DatabaseConnection("jdbc/S2000_PanDS");
        new DatabaseConnection(getDatabaseName());
        
        try
        {
            try 
            {
                conn = databaseConnection.getConnection();
            } 
            catch (SQLException e) 
            {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                e.printStackTrace(new PrintStream(os));
                throw new DatabaseException(e.getClass().getName(), 
                                            os.toString(), 
                                            "Error while trying to get SQL Connection from DataSource.");
            } 
            catch (NamingException e) 
            {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                e.printStackTrace(new PrintStream(os));
                throw new DatabaseException(e.getClass().getName(), 
                                                    os.toString(), 
                                                "Error while trying to locate DataSource.");
            }
            
            
            //skrifa logg
             try
             {
                writeLog(conn, "getUsers", ClID, IP, MacAdd, ClVersion);
             }
             catch(Exception e)
             {
                e.printStackTrace();
             }
             
            //S�kja g�gn
            
            int nFjoldi = 0;
            
            String sSQL = "";
            
            try 
            {     
                sSQL =  "select notandi_numer, hopur_numer, kerfi_numer, to_char(sysdate, 'dd.MM.yyyy dd:mm:hh') as ftimestamp,"+
                        "notandi_kennitala, notandi_nafn, utgafa, askrift_gildir_til " + 
                        "from kjarni.v_ofeigur_notandi " +
                        //"where kennimark_hash = '" + ClID + "'";
                        "where kennimark_hash = ? "; //���
                        
                pstmt = conn.prepareStatement(
                    sSQL,
                    java.sql.ResultSet.TYPE_SCROLL_SENSITIVE, 
                    java.sql.ResultSet.CONCUR_READ_ONLY);
                
                pstmt.setString(1, ClID); //���
                    
                rsGogn = pstmt.executeQuery();
                //fj�lda f�rslna
                rsGogn.last();
                nFjoldi = rsGogn.getRow();
                rsGogn.beforeFirst();           
            } 
            catch (SQLException e) 
            {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                e.printStackTrace(new PrintStream(os));
                throw new DatabaseException(e.getClass().getName(), 
                                            os.toString(), 
                                            "Error while preparing search statement.");
            }
    
            try 
            {  
                if(nFjoldi == 0)
                    throw new DataNotFoundException("Could not find any data based on your search input.");
                
                int nLoop = 0;
                users = new User[nFjoldi];
                
                while(rsGogn.next() && nLoop <= nFjoldi)
                {   
                    //public User(  int userID, int groupID, int systemID, String userSSID, String userName, 
                    //              String expireDate, String ofeigurVersion, String timestamp)        
                    users[nLoop] = new User(
                        rsGogn.getInt("notandi_numer"),
                        rsGogn.getInt("hopur_numer"),
                        rsGogn.getInt("kerfi_numer"),
                        rsGogn.getString("notandi_kennitala"),
                        rsGogn.getString("notandi_nafn"),
                        rsGogn.getString("askrift_gildir_til"), 
                        rsGogn.getString("utgafa"), 
                        rsGogn.getString("ftimestamp")
                    );     
                    nLoop++;
                }
            }
            catch (SQLException e) 
            {
                throw new DataNotFoundException("Could not find any data based on your search input.");
            }
    
            try 
            {
                databaseConnection.close();
            } 
            catch (SQLException e) 
            {
                throw new DataNotFoundException("Could not close databaseconnection: " + 
                                    e.getMessage());
                //gogn.setWarning("Could not close databaseconnection: " + 
                  //                e.getMessage());
    
            }
        }
        finally
        {
            gangaFraFinally(rsGogn, pstmt, conn);
        }

         return users;
    }

    
    /*
     * getFarms() - S�kir �ll d�r i hj�r� � �kv. b�li � opnu framlei�slu�ri
     * inn: client id ClID, h�pur n�mer GroupID, kerfi n�mer SystemID
     * �t: fylki me� �llum b�um + n�nari uppl�singum um �au
     **/

    //---------------------------------------------------------------

    @WebMethod
    public Farm[] getFarms(String clientID, int userID, int systemID) 
                                                     throws AuthenticationException, 
                                                               DatabaseException, 
                                                               DataNotFoundException, 
                                                               UnknownException//, 
                                                               //RemoteException
    {
        Connection conn = null;
        Farm[] farms = null;
        ResultSet rsGogn = null;
        PreparedStatement pstmt = null;
        
        DatabaseConnection databaseConnection = 
        /*Qurinus*///new DatabaseConnection("jdbc/bondiDS");
        /*Pan*///new DatabaseConnection("jdbc/S2000_PanDS");
        new DatabaseConnection(getDatabaseName());
       
        try 
        {
            
            try 
            {
                conn = databaseConnection.getConnection();
            } 
            catch (SQLException e) 
            {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                e.printStackTrace(new PrintStream(os));
                throw new DatabaseException(e.getClass().getName(), 
                                            os.toString(), 
                                            "Error while trying to get SQL Connection from DataSource.");
            } 
            catch (NamingException e) 
            {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                e.printStackTrace(new PrintStream(os));
                throw new DatabaseException(e.getClass().getName(), 
                                                    os.toString(), 
                                                "Error while trying to locate DataSource.");
            }
            
            //au�kenning
            try
            {     
                if(!getAuthentication(conn, userID, clientID, systemID))
                {
                    throw new AuthenticationException("Username, password and system tuple is invalid.");
                }
            }
            catch(SQLException e)
            {
                e.printStackTrace();
            }
            
            //skrifa logg
            writeLog(conn, "getFarms", clientID, null, null, null);
            
            //S�kja g�gn
            int nFjoldi = 0;
            
            String sSQL = "";
            
            try 
            {     
                sSQL =  "select bu_numer, busnumer, heiti, svfn_sveitarfelag, hreppur, baejarnumer, umrad_kennitala, " + 
                        "umrad_nafn, umrad_heimilisfang, umrad_postnumer, umrad_stadur, umrad_netfang, " + 
                        "bugrein_numer, skh_numer, uppgjor_numer, uppgjorsnumer, uppgjor_bu_heiti, uppgjor_persona_nafn, " + 
                        "landskodi_numer, opid_framleidsluar "+
                        "from kjarni.v_ofeigur_notandi_bu "+
                        //"where kennimark_hash = '" + clientID + "' " +
                        //"and kerfi_numer = " + systemID + 
                        "where kennimark_hash = ? " + //���
                        "and kerfi_numer = ? " + //���
                        "order by heiti";
                        
    
                pstmt = conn.prepareStatement(
                    sSQL,
                    java.sql.ResultSet.TYPE_SCROLL_SENSITIVE, 
                    java.sql.ResultSet.CONCUR_READ_ONLY);
                    
                pstmt.setString(1, clientID); //���
                pstmt.setInt(2, systemID); //���
                rsGogn = pstmt.executeQuery();
                
                //fj�lda f�rslna
                rsGogn.last();
                nFjoldi = rsGogn.getRow();
                rsGogn.beforeFirst();           
            } 
            catch (SQLException e) 
            {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                e.printStackTrace(new PrintStream(os));
                throw new DatabaseException(e.getClass().getName(), 
                                            os.toString(), 
                                            "Error while preparing search statement.");
            }
    
            try 
            {  
                if(nFjoldi == 0)
                    throw new DataNotFoundException("Could not find any data based on your search input.");
                
                int nLoop = 0;
                farms = new Farm[nFjoldi];
                
                while(rsGogn.next() && nLoop <= nFjoldi)
                {   
                    //public Farm( int farmSeqNumber, int farmID, String farmName, String sheepFarmID, String holdingID,      
                    //             int holdingTypeID, String communeNumName, String communeName, String keeperID, String keeperName,     
                    //             String keeperAddress, String keeperZip, String keeperLocation, String keeperEmail, int groupSeqNumber,    
                    //             int groupID, String groupKeeperName, String groupFarmName, int productionYear, int countryCode)       
                     
                    
                    farms[nLoop] = new Farm(
                        rsGogn.getInt("bu_numer"),
                        rsGogn.getInt("busnumer"),
                        rsGogn.getString("heiti"),        
                        rsGogn.getString("baejarnumer"),
                        rsGogn.getString("skh_numer"), 
                        rsGogn.getInt("bugrein_numer"),             
                        rsGogn.getString("svfn_sveitarfelag"),
                        rsGogn.getString("hreppur"),                
                        rsGogn.getString("umrad_kennitala"), 
                        rsGogn.getString("umrad_nafn"), 
                        rsGogn.getString("umrad_heimilisfang"), 
                        rsGogn.getString("umrad_postnumer"), 
                        rsGogn.getString("umrad_stadur"), 
                        rsGogn.getString("umrad_netfang"),                     
                        rsGogn.getInt("uppgjor_numer"),
                        rsGogn.getInt("uppgjorsnumer"),                    
                        rsGogn.getString("uppgjor_persona_nafn"), 
                        rsGogn.getString("uppgjor_bu_heiti"), 
                        rsGogn.getInt("opid_framleidsluar"),
                        rsGogn.getInt("landskodi_numer")                
                        );     
                    nLoop++;
                }
            }
            catch (SQLException e) 
            {
                //System.out.println(e);
                throw new DataNotFoundException("Could not find any data based on your search input.");
            }
    
            try 
            {
                databaseConnection.close();
            } 
            catch (SQLException e) 
            {
                throw new DataNotFoundException("Could not close databaseconnection: " + 
                                    e.getMessage());
                //gogn.setWarning("Could not close databaseconnection: " + 
                  //                e.getMessage());
    
            }
        }
        finally
        {
            gangaFraFinally(rsGogn, pstmt, conn);
        }
       
        return farms;
    }    
    

   /* -------------------------------------------------------------------   
    * S�kir Thungat�lur hjar�arinnar, l�mb f�dd � framleidslu�ri, lifandi �r og lifandi hr�tar.
    * GetLifeWeightData(...)
    * N�r � l�f�unga allra gripa sem eru � l�fi innan gildandi b�lis og framlei�slu�rs
    */

    @WebMethod
    public ThungiData[] getLifeWeightData(int userID, String clientID, 
                                  int systemID, 
                                  int farmID) throws AuthenticationException, 
                                                     DatabaseException, 
                                                     DataNotFoundException,
                                                     UnknownException
    {
        Connection conn = null;
        PreparedStatement pstmt = null;
        PreparedStatement pstmtTh = null;
        ResultSet rsGogn = null;
        ResultSet rsGognTh = null;
        ThungiData[] gogn = null;
        Thungi [] gognTh = null;
        int nFramleidsluar = 0;
        
        DatabaseConnection databaseConnection = 
        /*Qurinus*///new DatabaseConnection("jdbc/bondiDS");
        /*Pan*///new DatabaseConnection("jdbc/S2000_PanDS");
        new DatabaseConnection(getDatabaseName());

        try
        {
            try {
                conn = databaseConnection.getConnection();
            } 
            
            catch (SQLException e) {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                e.printStackTrace(new PrintStream(os));
                throw new DatabaseException(e.getClass().getName(), os.toString(), 
                                            "Error while trying to get SQL Connection from DataSource.");
            } 
            
            catch (NamingException e) {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                e.printStackTrace(new PrintStream(os));
                throw new DatabaseException(e.getClass().getName(), os.toString(), 
                                            "Error while trying to locate DataSource.");
            }
            //Au�kenning
            try {
    
                if (!getAuthentication(conn, userID, clientID, systemID)) {
                    throw new AuthenticationException("Username, password and system tuple is invalid.");
                }
            } 
            
            catch (SQLException e) {
                e.printStackTrace();
            }
    
            //skrifa logg
            writeLog(conn, "getThungi", clientID, null, null, null);
            
            //s�ki framlei�slu�r ���
            try
            {
              nFramleidsluar = getProductionYear(conn, farmID);
            }
            catch(SQLException e)
            {
              e.printStackTrace();
            }
    
            //S�kja g�gn
    
            int nFjoldi = 0;
            int nFjoldiTh;
            
            String sSQL;
            String sSQLTh;
            int nEinstaklingur;
            
            try {
                sSQL = 
                    "select BUSNUMER, EINSTAKLINGUR_NUMER, VALNR, LAMBANUMER, " +
                    "ORMERKI, NAFN, LITUR, KYN, S_FAEDINGARDAGS, " +
                    "FADIR_NUMER, FADIR_NAFN, FADIR_LITUR, FADIR_VALNR, " +
                    "MODIR_NUMER, MODIR_NAFN, MODIR_LITUR, MODIR_VALNR, " + 
                    "BLUP_FITA, BLUP_GERD, BLUP_MJOLK, BLUP_FRJOSEMI " +
                    "from v_hjord " + 
                    //"where byli_numer = " + farmID + 
                    "where byli_numer = ? " +
                    //b�tti vi� where m.v. comment ���
                    "and framleidsluar_numer = ? " + //���
                    "and stada_numer = ? " + //���
                    "order by rodun, NVL(valnr, lpad(lambanumer, 4, '0')) ";
    
                pstmt = conn.prepareStatement(sSQL, java.sql.ResultSet.TYPE_SCROLL_SENSITIVE, 
                                          java.sql.ResultSet.CONCUR_READ_ONLY);
                
                pstmt.setInt(1, farmID);
                pstmt.setInt(2, nFramleidsluar); //framlei�sluar ���
                pstmt.setInt(3, 0); //lifandi ���
                rsGogn = pstmt.executeQuery();
    
                //fj�lda f�rslna
                rsGogn.last();
                nFjoldi = rsGogn.getRow();
                rsGogn.beforeFirst();
                
        //            pstmt.close();
    
            } 
            
            catch (SQLException e) {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                e.printStackTrace(new PrintStream(os));
                throw new DatabaseException(e.getClass().getName(), os.toString(), 
                                            "Error while preparing search statement.");
            }
    
            try {
                if (nFjoldi == 0)
                    throw new DataNotFoundException("Could not find any data based on your search input.");
    
                int nLoop = 0;
                gogn = new ThungiData[nFjoldi];
                
                while(rsGogn.next() && nLoop <= nFjoldi)
                {
    
                    nEinstaklingur = rsGogn.getInt("einstaklingur_numer");
                    
                    nFjoldiTh = 0;
                    gognTh = null;
                    rsGognTh = null;
                    
                    try {
                        sSQLTh = 
                        "select TO_CHAR(t.DAGSETNING, 'dd.mm.rrrr') as DAGSETNING, " +
                        "t.THUNGIKG, t.FAEDINGARTHUNGI "+
                        "from V_OFEIGUR_THUNGI t " +
                        //"where t.EINSTAKLINGUR_NUMER = " + nEinstaklingur +
                        "where t.EINSTAKLINGUR_NUMER = ? "+
                        "order by t.dagsetning";
                        
                        pstmtTh = conn.prepareStatement(sSQLTh, java.sql.ResultSet.TYPE_SCROLL_SENSITIVE, 
                                                  java.sql.ResultSet.CONCUR_READ_ONLY);
                        pstmtTh.setInt(1, nEinstaklingur);
    
                        rsGognTh = pstmtTh.executeQuery();
        //                    pstmtTh.close();
    
                        //fj�lda f�rslna
                        rsGognTh.last();
                        nFjoldiTh = rsGognTh.getRow();
                        rsGognTh.beforeFirst();
                        
                        } 
                        catch (SQLException e) {
                            ByteArrayOutputStream os = new ByteArrayOutputStream();
                            e.printStackTrace(new PrintStream(os));
                            throw new DatabaseException(e.getClass().getName(), os.toString(), 
                                                        "Error while preparing search statement.");
                        }                    
    
                        if (nFjoldiTh > 0) {                    
    
                            try {
                            
                                 int nLoopTh = 0;
                                gognTh = new Thungi [nFjoldiTh];
                            
                                while(rsGognTh.next() && nLoopTh <= nFjoldiTh)
                                {
    
                                Thungi EinstThungi = new Thungi (
                                    rsGognTh.getString("dagsetning"),
                                    rsGognTh.getString("thungikg"),
                                    rsGognTh.getString("faedingarthungi")
                                );
    
                                gognTh[nLoopTh] = EinstThungi;
                                
                                nLoopTh ++;
                                }                   
                            }  
                        
                            catch (SQLException e)
                            {
                            throw new DataNotFoundException("Could not find any data based on your search input.");
                            }
                        }
    
                   
    /*                ThungiData HjordThungi = new ThungiData(
    //                    rsGogn.getInt("busnumer"), 
    //                    rsGogn.getInt("einstaklingur_numer"),
    //                    rsGogn.getString("valnr"),
                        rsGogn.getString("lambanumer"),
    //                    rsGogn.getString("ormerki"), 
                        rsGogn.getString("nafn"),
                        rsGogn.getInt("litur"),
    //                    rsGogn.getInt("kyn"),
                        rsGogn.getString("s_faedingardags"),
                        rsGogn.getInt("fadir_numer"),
                        rsGogn.getString("fadir_valnr"),                    
                        rsGogn.getString("fadir_nafn"),
                        rsGogn.getInt("fadir_litur"),
                        rsGogn.getInt("modir_numer"),
                        rsGogn.getString("modir_valnr"),                    
                        rsGogn.getString("modir_nafn"),
                        rsGogn.getInt("modir_litur"),
                        rsGogn.getInt("blup_fita"), 
                        rsGogn.getInt("blup_gerd"),
                        rsGogn.getInt("blup_mjolk"),
                        rsGogn.getInt("blup_frjosemi"),
                        gognTh
                        );
                         
                      gogn[nLoop] = HjordThungi;  
    */                  
                    if (pstmtTh != null)
                    {
                        pstmtTh.close();
                        pstmtTh = null;
                    }
                    
                    if (rsGognTh != null)
                    { 
                        rsGognTh.close();
                        rsGognTh = null;
                    }
                    
                     nLoop++;                
    
                 }
            } 
                catch (SQLException e)
                {
                    throw new DataNotFoundException("Could not find any data based on your search input.");
                }
        
    
                try
                {
                    if(pstmt != null)
                    {
                        pstmt.close();
                        pstmt = null;
                    }
                    
                    if(pstmtTh != null)
                    {
                        pstmtTh.close();
                        pstmtTh = null;
                    }
                    
                    if(rsGogn != null) {
                        rsGogn.close();
                        rsGogn = null;
                    }
                    
                    if(rsGognTh != null) {
                        rsGognTh.close();
                        rsGognTh = null;
                    }
                    
                    if(conn != null) {
                        conn.close();
                        conn = null;
                    }
                
                    
                //databaseConnection.close();
                }
                catch (SQLException e)
                {
                throw new DataNotFoundException("Could not close databaseconnection: " + 
                                    e.getMessage());
                //gogn.setWarning("Could not close databaseconnection: " + 
                  //                e.getMessage());
    
            }
        }
        finally
        {
            gangaFraFinally(rsGogn, pstmt, conn);
        }

        return gogn;

    }
    
    //----------------------------------------------------------------------------    
        
    
    /* pushLifeWeightData()
     * inn: int userID, String clientID, int systemID, int farmID
     * út: fylki
     **/

    @WebMethod
    public RFIDIn pushLifeWeightData(int userID, String clientID, int systemID, int farmID,
                                        ThungiPrGrip thungiPrGrip) 
                                                 throws  AuthenticationException, 
                                                         DatabaseException, 
                                                         DataNotFoundException, 
                                                         UnknownException 
        {

            RFIDIn inn = new RFIDIn();
            
            Connection conn = null;
            CallableStatement cstmt = null;
            
            DatabaseConnection databaseConnection = 
            /*Qurinus*///new DatabaseConnection("jdbc/bondiDS");
            /*Pan*///new DatabaseConnection("jdbc/S2000_PanDS");
            new DatabaseConnection(getDatabaseName());
         
            try
            {
                try 
                {
                    conn = databaseConnection.getConnection();
                } 
                catch (SQLException e) 
                {
                    ByteArrayOutputStream os = new ByteArrayOutputStream();
                    e.printStackTrace(new PrintStream(os));
                    throw new DatabaseException(e.getClass().getName(), 
                                                os.toString(), 
                                                "Error while trying to get SQL Connection from DataSource.");
                }
                catch (NamingException e) 
                {
                    ByteArrayOutputStream os = new ByteArrayOutputStream();
                    e.printStackTrace(new PrintStream(os));
                    throw new DatabaseException(e.getClass().getName(), 
                                                os.toString(), 
                                                "Error while trying to locate DataSource.");
                }
                
                //au?kenning
                try
                {
                    if (!getAuthentication(conn, userID, clientID, systemID)) 
                    {
                        throw new AuthenticationException("Username, password and system tuple is invalid.");
                    }
                }
                catch(SQLException e)
                {
                    e.printStackTrace();
                }
                
                //skrifa logg
                writeLog(conn, "pushLifeWeightData", clientID, null, null, null);
                
    
                try {
                    conn.setAutoCommit(false);
                }
                catch (SQLException e) 
                {
                    inn.setWarning("Could not set connection to not autocommit: " + 
                                           e.getMessage());
                }
    
                int nVilla;
                
                try 
                {          
                    // Ef veri? er a? kalla ? function en ekki procedure m? nota: { call ? := setInngogn(?,?,?,?,?) }
                    // spOrmerki(
                        //1) p_gripur_numer IN NUMBER, 
                        //2) p_ormerki IN VARCHAR2, 
                        //3) p_framleidsluartal IN NUMBER,
                        //4) p_skrad_af IN NUMBER, 
                        //5) p_villa OUT NUMBER)
    
                
                    // Lesa fyrst inn m?ina
                    cstmt = conn.prepareCall("{ call pk_ofeigur.InsertThungi(?,?,?,?,?,?,?)}");
                                        
                    cstmt.setInt(1, userID);
                    cstmt.setInt(2, farmID);
                    cstmt.setInt(3, thungiPrGrip.getEinstaklingurNumer());
                    cstmt.setString(4, thungiPrGrip.getDagsetning());
                    cstmt.setString(5, thungiPrGrip.getThungiKg());
                    cstmt.setString(6, thungiPrGrip.getFaedingarthungi());
                    
                    cstmt.registerOutParameter(7, Types.INTEGER);
                    
                    cstmt.execute();
    
                    nVilla = cstmt.getInt(7);                
    
                    if(nVilla != 0)
                     {//me�h�ndlun villu, sett inn 04.10.2010, IAR.
                         String sVilla = "";
                         CallableStatement cstmtVilla = 
                         conn.prepareCall("{ call ? := fn_get_villa(?)}");
                         cstmtVilla.registerOutParameter(1, Types.VARCHAR);
                         cstmtVilla.setInt(2, nVilla);
                         cstmtVilla.execute();
                         sVilla = cstmtVilla.getString(1);
                         cstmtVilla.close();
                         //System.out.println(nVilla);
    
                        inn.setWarning(sVilla);
                     }
    
                    inn.setResult(nVilla);
                    
                    //System.out.println(nVilla);            
                    if(nVilla == 0)
                        conn.commit();
                    else
                        conn.rollback();
                } 
                catch (SQLException e) 
                {
                    ByteArrayOutputStream os = new ByteArrayOutputStream();
                    e.printStackTrace(new PrintStream(os));
                    throw new DatabaseException(e.getClass().getName(), 
                                                os.toString(), 
                                                "Error while preparing update call.");
                }   
                //catch(DatabaseException ex){
                //    ex.getMessage();
                //}
    
                try 
                {
                    databaseConnection.close();
                } 
                catch (SQLException e) 
                {
                    inn.setWarning("Could not close databaseconnection: " + 
                                           e.getMessage());
                }
            }
            finally
            {
                gangaFraFinally(cstmt, conn);
            }
            return inn;
        }    
    

    /*S�kja gripi
                   * */   
       
     //**************************************************************************
     
     public boolean getAuthentication(Connection conn, int UserID, String ClID, int SystemID) 
         throws SQLException, AuthenticationException
     {
         boolean bAuthenticated = false;
         String sSQL = "";
         PreparedStatement pStmt = null;
         ResultSet rsResult = null;
         
         try 
         {
             sSQL =  "select notandi_numer, kennimark_hash, kerfi_numer " +
                     "from kjarni.v_ofeigur_notandi " +
                     "where notandi_numer = ? " +
                     "and kennimark_hash = ? " +
                     "and kerfi_numer = ? ";
                      
             pStmt = conn.prepareStatement(sSQL);
             
             pStmt.setInt(1, UserID);
             pStmt.setString(2, ClID);
             pStmt.setInt(3, SystemID);
             
             rsResult= pStmt.executeQuery();
             if(rsResult.next())
                 bAuthenticated  = true;
             else
                 bAuthenticated = false;
               
             pStmt.close();
             rsResult.close();
             pStmt = null;
             rsResult = null;    
         } 
         catch (Exception e) 
         {
             e.printStackTrace();
             throw new AuthenticationException("Database error while authenticating user.");
         }
         finally
         {
            
             if(rsResult != null)
             {
                 rsResult.close();
                 rsResult = null;
             }   
             if(pStmt != null)
             { 
                 pStmt.close();
                 pStmt = null;
             }
         }
         return bAuthenticated;
     }
     
     public void writeLog(Connection conn, String sLogText, String sClID, String IPAddress, String MacAddress, String ClVersion)
     {   
         CallableStatement cstmt = null;
         
         try 
         {
             InetAddress addr = InetAddress.getLocalHost();
             byte[] ipAddr = addr.getAddress();    
             String ipAddrStr = ""; 
             for(int i=0; i >= ipAddr.length; i++) 
             { 
                 if (i > 0)  
                     ipAddrStr += "."; 
                 ipAddrStr += ipAddr[i]&0xFF; 
             } 
           
             cstmt = conn.prepareCall("{ call kjarni.p_ofeigur_insert_log(?,?,?,?,?,?)}");
         
             cstmt.setString(1, sClID);
             cstmt.setString(2, IPAddress);
             cstmt.setString(3, MacAddress);
             cstmt.setString(4, ClVersion);
             cstmt.setString(5, ipAddrStr);
             cstmt.setString(6, sLogText);
             cstmt.execute();
             
             conn.commit();
         } 
         catch(UnknownHostException ex)
         {
             ex.printStackTrace();
         }
         catch (SQLException e) 
         {
             e.printStackTrace();
             //ByteArrayOutputStream os = new ByteArrayOutputStream();
             //e.printStackTrace(new PrintStream(os));
             //throw new DatabaseException(e.getClass().getName(), 
                //                         os.toString(), 
                  //                       "Error while preparing update call.");
         }
         finally 
         {
            gangaFraFinally(cstmt, null);
         }
         
     }
     
    /*
     * getHeard() - s�kir �ll b� �kve�ins notanda
     * inn: client id ClID, h�pur n�mer GroupID, kerfi n�mer SystemID
     * �t: fylki me� �llum b�um + n�nari uppl�singum um �au
     **/

     //---------------------------------------------------------------
      @WebMethod
      public AnimalIds[] getHeard(String clientID, int userID, int systemID, int farmID) 
                                              throws  AuthenticationException, 
                                                      DatabaseException, 
                                                      DataNotFoundException, 
                                                      UnknownException 
      {
          Connection conn = null;
          PreparedStatement pstmt = null;
          PreparedStatement pstmtFjarfelag = null;
          ResultSet rsGognFjarfelag = null;
          ResultSet rsGogn = null;
          AnimalIds[] hjord = null;
          
          DatabaseConnection databaseConnection = 
          /*Qurinus*///new DatabaseConnection("jdbc/bondiDS");
          /*Pan*///new DatabaseConnection("jdbc/S2000_PanDS");
          new DatabaseConnection(getDatabaseName());
           
          try
          {
              try 
              {
                  conn = databaseConnection.getConnection();
              } 
              catch (SQLException e) 
              {
                  ByteArrayOutputStream os = new ByteArrayOutputStream();
                  e.printStackTrace(new PrintStream(os));
                  throw new DatabaseException(e.getClass().getName(), 
                                              os.toString(), 
                                              "Error while trying to get SQL Connection from DataSource.");
              } 
              catch (NamingException e) 
              {
                  ByteArrayOutputStream os = new ByteArrayOutputStream();
                  e.printStackTrace(new PrintStream(os));
                  throw new DatabaseException(e.getClass().getName(), 
                                                      os.toString(), 
                                                  "Error while trying to locate DataSource.");
              }
              
              //au�kenning
              try
              {     
                  if(!getAuthentication(conn, userID, clientID, systemID))
                  {
                      throw new AuthenticationException("Username, password and system tuple is invalid.");
                  }
              }
              catch(SQLException e)
              {
                  e.printStackTrace();
              }          
              
              
              int nFjoldi = 0;
                        
              String sSQL = "";  
              int nFjarfelagsbuNumer = 0;          
              
             // --------------------------------  
             
              try {
                  sSQL = "SELECT DISTINCT fjarfelagsbunumer " +
                         "FROM v_ofeigur_uppgjor " +
                         //"WHERE byli_numer = " + farmID + 
                         "where byli_numer = ? " + //���
                         "AND fjarfelagsbunumer IS NOT NULL ";
    
                  pstmtFjarfelag = 
                       conn.prepareStatement(
                  sSQL,
                  java.sql.ResultSet.TYPE_SCROLL_SENSITIVE,
                  java.sql.ResultSet.CONCUR_READ_ONLY );
                  
              pstmtFjarfelag.setInt(1, farmID); //���
    
               rsGognFjarfelag = pstmtFjarfelag.executeQuery();
              
              //fj�ldi f�rslna
              rsGognFjarfelag.last();
              nFjoldi = rsGognFjarfelag.getRow();
              rsGognFjarfelag.beforeFirst();    
    
              }
              catch (SQLException e) 
              {
                  ByteArrayOutputStream os = new ByteArrayOutputStream();
                  e.printStackTrace(new PrintStream(os));
                  throw new DatabaseException(e.getClass().getName(), 
                                              os.toString(), 
                                              "Error while preparing search statement.");
              }
    
              try 
              {  
                  if(nFjoldi == 0)
                      throw new DataNotFoundException("Could not find any data based on your search input.");
                  
                  int nLoop = 0;
                  
                  while(rsGognFjarfelag.next() && nLoop <= nFjoldi)
                  {  
                       nFjarfelagsbuNumer = rsGognFjarfelag.getInt("fjarfelagsbunumer");  
                      nLoop++;
                  }
              }
              catch (SQLException e) 
              {
                  //System.out.println(e);
                  throw new DataNotFoundException("Could not find any data based on your search input.");
              }
            // -------------------------------
             sSQL = "";
             
             try 
             {
                  sSQL = 
                          "SELECT * FROM (SELECT einstaklingur_numer, kyn, NVL(valnr, lambanumer) valnr, ormerki, tegund " + 
                          "FROM v_ofeigur_hjord_opid_ar " + 
                          //"WHERE byli_numer = " + farmID + 
                          "WHERE byli_numer = ? " + //���
                          " UNION " + 
                          "SELECT einstaklingur_numer, kyn, valnr, ormerki, tegund " + 
                          "FROM v_ofeigur_hjord_hrutar " + 
                          //"WHERE (fjarfelagsbu_numer = " + nFjarfelagsbuNumer + 
                          "WHERE (fjarfelagsbu_numer = ? " + //���
                          " OR " + 
                          //"saedingabu_numer IS NOT NULL) AND NVL(heimabu_numer, 0) != " + farmID + 
                          "saedingabu_numer IS NOT NULL) AND NVL(heimabu_numer, 0) != ? " + //��� 
                          ") ORDER BY tegund, valnr";
    
                    pstmt = conn.prepareStatement(
                     sSQL,
                     java.sql.ResultSet.TYPE_SCROLL_SENSITIVE,
                     java.sql.ResultSet.CONCUR_READ_ONLY);
                     
                  pstmt.setInt(1, farmID); //���
                  pstmt.setInt(2, nFjarfelagsbuNumer); //���
                  pstmt.setInt(3, farmID); //���
     
                  rsGogn = pstmt.executeQuery();
    
                  //fj�ldi f�rslna
                 rsGogn.last();
                 nFjoldi = rsGogn.getRow();
                 rsGogn.beforeFirst();           
             } 
             catch (SQLException e) 
             {
                 ByteArrayOutputStream os = new ByteArrayOutputStream();
                 e.printStackTrace(new PrintStream(os));
                 throw new DatabaseException(e.getClass().getName(), 
                                             os.toString(), 
                                             "Error while preparing search statement.");
             }
    
             try 
             {  
                 if(nFjoldi == 0)
                     throw new DataNotFoundException("Could not find any data based on your search input.");
                 
                 int nLoop = 0;
                 hjord = new AnimalIds[nFjoldi];
                 
                 while(rsGogn.next() && nLoop <= nFjoldi)
                 {  
                     
                     hjord[nLoop] = new AnimalIds(
                         rsGogn.getInt("einstaklingur_numer"),
                         rsGogn.getInt("kyn"),
                         rsGogn.getString("valnr"),        
                         rsGogn.getString("ormerki"),
                         rsGogn.getInt("tegund")
                         );     
                     nLoop++;
                 }
             }
             catch (SQLException e) 
             {
                 //System.out.println(e);
                 throw new DataNotFoundException("Could not find any data based on your search input.");
             }
    
             try 
             {
                 databaseConnection.close();
             } 
             catch (SQLException e) 
             {
                 throw new DataNotFoundException("Could not close databaseconnection: " + 
                                     e.getMessage());
                 //gogn.setWarning("Could not close databaseconnection: " + 
                   //                e.getMessage());
    
             } 
          }
          finally 
          {
                gangaFraFinally(rsGogn, pstmt, conn);
                gangaFraFinally(rsGognFjarfelag, pstmtFjarfelag, null);
              
          }
        // -------------------------------
          return hjord;
      }     

       
    //**************************************************************************
    @WebMethod    
    public int getRFIdInfo(String clientID, int userID, int systemID, String RFId) 
       throws AuthenticationException, 
              DatabaseException, 
              DataNotFoundException, 
              UnknownException//, 
             //RemoteException
    {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rsGogn = null;
        int nFarmId = 0;
        
        DatabaseConnection databaseConnection = 
        /*Qurinus*///new DatabaseConnection("jdbc/bondiDS");
        /*Pan*///new DatabaseConnection("jdbc/S2000_PanDS");
        new DatabaseConnection(getDatabaseName());
       
        try
        {
           try 
           {
           conn = databaseConnection.getConnection();
              } 
              catch (SQLException e) 
              {
                  ByteArrayOutputStream os = new ByteArrayOutputStream();
                  e.printStackTrace(new PrintStream(os));
                  throw new DatabaseException(e.getClass().getName(), 
                                              os.toString(), 
                                              "Error while trying to get SQL Connection from DataSource.");
              } 
              catch (NamingException e) 
              {
                  ByteArrayOutputStream os = new ByteArrayOutputStream();
                  e.printStackTrace(new PrintStream(os));
                  throw new DatabaseException(e.getClass().getName(), 
                                                      os.toString(), 
                                                  "Error while trying to locate DataSource.");
              }
              
              //au�kenning
              try
              {     
                  if(!getAuthentication(conn, userID, clientID, systemID))
                  {
                      throw new AuthenticationException("Username, password and system tuple is invalid.");
                  }
              }
              catch(SQLException e)
              {
                  e.printStackTrace();
              }
              
              //skrifa logg
              writeLog(conn, "getRFIdInfo", clientID, null, null, null);
              
              //S�kja g�gn
              
              int nFjoldi = 0;
                        
              String sSQL = "";
              
              try 
              {     
                  sSQL =  "SELECT e.byli_numer " + 
                          "FROM v_ofeigur_ormerki_bu o, einstaklingur e " +
                          "WHERE o.gripur_numer = e.numer " +
                          //"AND o.ormerki = '" + RFId + "'";
                          "AND o.ormerki = ? "; //���
                          
    
                  pstmt = conn.prepareStatement(
                      sSQL,
                      java.sql.ResultSet.TYPE_SCROLL_SENSITIVE, 
                      java.sql.ResultSet.CONCUR_READ_ONLY);
                      
                  pstmt.setString(1, RFId); //���
                  
                      
                  rsGogn = pstmt.executeQuery();
                  
                  //fj�ldi f�rslna
                  rsGogn.last();
                  nFjoldi = rsGogn.getRow();
                  rsGogn.beforeFirst();           
              } 
              catch (SQLException e) 
              {
                  ByteArrayOutputStream os = new ByteArrayOutputStream();
                  e.printStackTrace(new PrintStream(os));
                  throw new DatabaseException(e.getClass().getName(), 
                                              os.toString(), 
                                              "Error while preparing search statement.");
              }
    
              try 
              {  
                  if(nFjoldi == 0)
                     nFarmId = 0;
    //                  throw new DataNotFoundException("Could not find any data based on your search input.");
                  
                  int nLoop = 0;
                  
                  while(rsGogn.next() && nLoop <= nFjoldi)
                  {  
                    nFarmId = rsGogn.getInt("byli_numer");
                      nLoop++;
                  }
              }
              catch (SQLException e) 
              {
                  //System.out.println(e);
                  throw new DataNotFoundException("Could not find any data based on your search input.");
              }
    
              try 
              {
                  databaseConnection.close();
              } 
              catch (SQLException e) 
              {
                  throw new DataNotFoundException("Could not close databaseconnection: " + 
                                      e.getMessage());
                  //gogn.setWarning("Could not close databaseconnection: " + 
                    //                e.getMessage());
    
              }
        }
        finally 
        {
              gangaFraFinally(rsGogn, pstmt, conn);      
        }
        
        return nFarmId;
    }    
      
    /* -------------------------------------------------------------------   
     * S�kir Thungat�lur hjar�arinnar, l�mb f�dd � framleidslu�ri, lifandi �r og lifandi hr�tar.
     */

     @WebMethod
     public ThungiData[] getLifeWeightPrAnimal(int userID, String clientID, 
                                   int systemID, 
                                   int AnimalId) throws AuthenticationException, 
                                                      DatabaseException, 
                                                      DataNotFoundException,
                                                      UnknownException
    {

         Connection conn = null;
         PreparedStatement pstmt = null;
         PreparedStatement pstmtTh = null;
         ResultSet rsGogn = null;
         ResultSet rsGognTh = null;

         ThungiData[] gogn = null;
         Thungi [] gognTh = null;
         
         DatabaseConnection databaseConnection = 
         /*Qurinus*///new DatabaseConnection("jdbc/bondiDS");
         /*Pan*///new DatabaseConnection("jdbc/S2000_PanDS");
         new DatabaseConnection(getDatabaseName());

        try
        {
             try {
                 conn = databaseConnection.getConnection();
             } 
             
             catch (SQLException e) {
                 ByteArrayOutputStream os = new ByteArrayOutputStream();
                 e.printStackTrace(new PrintStream(os));
                 throw new DatabaseException(e.getClass().getName(), os.toString(), 
                                             "Error while trying to get SQL Connection from DataSource.");
             } 
             
             catch (NamingException e) {
                 ByteArrayOutputStream os = new ByteArrayOutputStream();
                 e.printStackTrace(new PrintStream(os));
                 throw new DatabaseException(e.getClass().getName(), os.toString(), 
                                             "Error while trying to locate DataSource.");
             }
             //Au�kenning
             try {

                 if (!getAuthentication(conn, userID, clientID, systemID)) {
                     throw new AuthenticationException("Username, password and system tuple is invalid.");
                 }
             } 
             
             catch (SQLException e) {
                 e.printStackTrace();
             }

             //skrifa logg
             writeLog(conn, "getThungi", clientID, null, null, null);


             //S�kja g�gn

             int nFjoldi = 0;
             int nFjoldiTh;
             
             String sSQL;
             String sSQLTh;
             int nEinstaklingur;
             
             try {
                 sSQL = 
                     "select LAMBANUMER, " +
                     " NAFN, LITUR, S_FAEDINGARDAGS, " +
                     "FADIR_NUMER, FADIR_NAFN, FADIR_LITUR, FADIR_VALNR, " +
                     "MODIR_NUMER, MODIR_NAFN, MODIR_LITUR, MODIR_VALNR, " + 
                     "BLUP_FITA, BLUP_GERD, BLUP_MJOLK, BLUP_FRJOSEMI, BURDUR " +
                     "from v_ofeigur_gripur " + 
                     //"where einstaklingur_numer = " + AnimalId;
                     "where einstaklingur_numer = ?"; //���
                     
                     /*                     "select BUSNUMER, EINSTAKLINGUR_NUMER, VALNR, LAMBANUMER, " +
                     "ORMERKI, NAFN, LITUR, KYN, S_FAEDINGARDAGS, " +
                     "FADIR_NUMER, FADIR_NAFN, FADIR_LITUR, FADIR_VALNR, " +
                     "MODIR_NUMER, MODIR_NAFN, MODIR_LITUR, MODIR_VALNR, " + 
                     "BLUP_FITA, BLUP_GERD, BLUP_MJOLK, BLUP_FRJOSEMI " +
                     "from v_ofeigur_gripur " + "where einstaklingur_numer = " + AnimalId;*/
         //               "order by ormerki";
//                     "order by rodun, NVL(valnr, lpad(lambanumer, 4, '0')) ";

                  pstmt = conn.prepareStatement(sSQL, java.sql.ResultSet.TYPE_SCROLL_SENSITIVE, 
                                           java.sql.ResultSet.CONCUR_READ_ONLY);
                                           
                  pstmt.setInt(1, AnimalId); //���

                 rsGogn = pstmt.executeQuery();

                 //fj�lda f�rslna
                 rsGogn.last();
                 nFjoldi = rsGogn.getRow();
                 rsGogn.beforeFirst();
                 
         //            pstmt.close();

             } 
             
             catch (SQLException e) {
                 ByteArrayOutputStream os = new ByteArrayOutputStream();
                 e.printStackTrace(new PrintStream(os));
                 throw new DatabaseException(e.getClass().getName(), os.toString(), 
                                             "Error while preparing search statement.");
             }

             try {
                 if (nFjoldi == 0)
                     throw new DataNotFoundException("Could not find any data based on your search input.");

                 int nLoop = 0;
                 gogn = new ThungiData[nFjoldi];
                 
                 while(rsGogn.next() && nLoop <= nFjoldi)
                 {

                     //nEinstaklingur = rsGogn.getInt("einstaklingur_numer");
                     nEinstaklingur = AnimalId;
                      
                     nFjoldiTh = 0;
                     gognTh = null;
                     rsGognTh = null;
                     
                     try {
                         sSQLTh = 
                         "select TO_CHAR(t.DAGSETNING, 'dd.mm.rrrr') as DAGSETNING, " +
                         "t.THUNGIKG, t.FAEDINGARTHUNGI "+
                         "from V_OFEIGUR_THUNGI t " +
                         //"where t.EINSTAKLINGUR_NUMER = " + nEinstaklingur +
                         "where t.EINSTAKLINGUR_NUMER = ? " + //���
                         "order by t.dagsetning";
                         
                         pstmtTh = conn.prepareStatement(sSQLTh, java.sql.ResultSet.TYPE_SCROLL_SENSITIVE, 
                                                   java.sql.ResultSet.CONCUR_READ_ONLY);
                                                   
                         pstmtTh.setInt(1, nEinstaklingur); //���

                         rsGognTh = pstmtTh.executeQuery();
         //                    pstmtTh.close();

                         //fj�lda f�rslna
                         rsGognTh.last();
                         nFjoldiTh = rsGognTh.getRow();
                         rsGognTh.beforeFirst();
                         
                         } 
                         catch (SQLException e) {
                             ByteArrayOutputStream os = new ByteArrayOutputStream();
                             e.printStackTrace(new PrintStream(os));
                             throw new DatabaseException(e.getClass().getName(), os.toString(), 
                                                         "Error while preparing search statement.");
                         }                    

                         if (nFjoldiTh > 0) {                    

                             try {
                             
                                  int nLoopTh = 0;
                                 gognTh = new Thungi [nFjoldiTh];
                             
                                 while(rsGognTh.next() && nLoopTh <= nFjoldiTh)
                                 {

                                 Thungi EinstThungi = new Thungi (
                                     rsGognTh.getString("dagsetning"),
                                     rsGognTh.getString("thungikg"),
                                     rsGognTh.getString("faedingarthungi")
                                 );

                                 gognTh[nLoopTh] = EinstThungi;
                                 
                                 nLoopTh ++;
                                 }                   
                             }  
                         
                             catch (SQLException e)
                             {
                             throw new DataNotFoundException("Could not find any data based on your search input.");
                             }
                         }

                    
                     ThungiData HjordThungi = new ThungiData(
//                         rsGogn.getInt("busnumer"), 
//                         rsGogn.getInt("einstaklingur_numer"),
//                         rsGogn.getString("valnr"),
                         rsGogn.getString("lambanumer"),
//                         rsGogn.getString("ormerki"), 
                         rsGogn.getString("nafn"),
                         rsGogn.getInt("litur"),
//                         rsGogn.getInt("kyn"),
                         rsGogn.getString("s_faedingardags"),
                         rsGogn.getInt("fadir_numer"),
                         rsGogn.getString("fadir_valnr"),                    
                         rsGogn.getString("fadir_nafn"),
                         rsGogn.getInt("fadir_litur"),
                         rsGogn.getInt("modir_numer"),
                         rsGogn.getString("modir_valnr"),                    
                         rsGogn.getString("modir_nafn"),
                         rsGogn.getInt("modir_litur"),
                         rsGogn.getInt("blup_fita"), 
                         rsGogn.getInt("blup_gerd"),
                         rsGogn.getInt("blup_mjolk"),
                         rsGogn.getInt("blup_frjosemi"),
                         rsGogn.getInt("burdur"),
                         gognTh
                         );
                          
                       gogn[nLoop] = HjordThungi;  
                       
                     if (pstmtTh != null)
                     {
                         pstmtTh.close();
                         pstmtTh = null;
                     }
                     
                     if (rsGognTh != null)
                     { 
                         rsGognTh.close();
                         rsGognTh = null;
                     }
                     
                      nLoop++;                

                  }
             } 
                 catch (SQLException e)
                 {
                     throw new DataNotFoundException("Could not find any data based on your search input.");
                 }
         

                 try
                 {
                     if(pstmt != null)
                     {
                         pstmt.close();
                         pstmt = null;
                     }
                     
                     if(pstmtTh != null)
                     {
                         pstmtTh.close();
                         pstmtTh = null;
                     }
                     
                     if(rsGogn != null) {
                         rsGogn.close();
                         rsGogn = null;
                     }
                     
                     if(rsGognTh != null) {
                         rsGognTh.close();
                         rsGognTh = null;
                     }
                     
                     if(conn != null) {
                         conn.close();
                         conn = null;
                     }
                 
                     
                 //databaseConnection.close();
                 }
                 catch (SQLException e)
                 {
                 throw new DataNotFoundException("Could not close databaseconnection: " + 
                                     e.getMessage());
                 //gogn.setWarning("Could not close databaseconnection: " + 
                   //                e.getMessage());

                 }

            }
            finally 
            {
                gangaFraFinally(rsGogn, pstmt, conn);      
                gangaFraFinally(rsGognTh, pstmtTh, null);      
            }
                 
            return gogn;

         }
         
         
    @WebMethod
    public BylisHrutur[] getRamData(int userID, String clientID, int systemID, int ramID) 
                                             throws 
                                                 AuthenticationException,
                                                 DatabaseException, 
                                                 DataNotFoundException, 
                                                 UnknownException
    {   
        Connection conn = null;
        ResultSet rsGogn = null;
        BylisHrutur[] gogn = null;
        PreparedStatement pstmt = null;
        
        DatabaseConnection databaseConnection = 
        /*Qurinus*///new DatabaseConnection("jdbc/bondiDS");
        /*Pan*///new DatabaseConnection("jdbc/S2000_PanDS");
        new DatabaseConnection(getDatabaseName());
        
        try
        {
            try 
            {
                conn = databaseConnection.getConnection();
            } 
            catch (SQLException e) 
            {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                e.printStackTrace(new PrintStream(os));
                throw new DatabaseException(e.getClass().getName(), 
                                            os.toString(), 
                                            "Error while trying to get SQL Connection from DataSource.");
            }
            catch (NamingException e) 
            {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                e.printStackTrace(new PrintStream(os));
                throw new DatabaseException(e.getClass().getName(), 
                                            os.toString(), 
                                            "Error while trying to locate DataSource.");
            }
            //Au?kenning
            try
            {
                
                if(!getAuthentication(conn, userID, clientID, systemID))
                {
                    throw new AuthenticationException("Username, password and system tuple is invalid.");
                }
            }
            catch(SQLException e)
            {
                e.printStackTrace();
            }
    
            //skrifa logg
            //  IP hva?a?ra h????? - athuga seinna
            writeLog(conn, "getRamData", clientID, null, null, null);
    
            //S?kja g?gn
                  
        //        RamDataOut[] gogn = null;
            int nFjoldi = 0;
            
            String sSQL;
            
            try 
            {     
                sSQL = "select busnumer, valnumer, ormerki, hrutur_numer, nafn, litur, " +
                       "blup_fita, blup_gerd, blup1_mjolkurlagni, blup1_frjosemi, " +
                       "blup2_mjolkurlagni, blup2_frjosemi, blup3_mjolkurlagni, " +
                       "blup3_frjosemi, blup4_mjolkurlagni, blup4_frjosemi, " +
                       "blup_einkunn_m, blup_einkunn_f, " +
                       "valnumer_ff, ormerki_ff, numer_ff, nafn_ff, litur_ff, " +
                       "valnumer_fff, ormerki_fff, numer_fff, nafn_fff, litur_fff, " +
                       "valnumer_mm, ormerki_mm, numer_mm, nafn_mm, litur_mm, " +
                       "valnumer_mmm, ormerki_mmm, numer_mmm, nafn_mmm, litur_mmm " +
                       "from v_ofeigur_hrutur " +
                       //"where hrutur_numer = " + ramID +
                       "where hrutur_numer = ? " + //���
                       "order by valnumer";
    
                pstmt = conn.prepareStatement(
                    sSQL,
                    java.sql.ResultSet.TYPE_SCROLL_SENSITIVE, 
                    java.sql.ResultSet.CONCUR_READ_ONLY);
                    
                pstmt.setInt(1, ramID); //���
                    
                rsGogn = pstmt.executeQuery();
                
                //fj?lda f?rslna
                rsGogn.last();
                nFjoldi = rsGogn.getRow();
                rsGogn.beforeFirst();
                
            } 
            catch (SQLException e) 
            {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                e.printStackTrace(new PrintStream(os));
                throw new DatabaseException(e.getClass().getName(), 
                                            os.toString(), 
                                            "Error while preparing search statement.");
            }
    
            try 
            {  
                if(nFjoldi == 0)
                    throw new DataNotFoundException("Could not find any data based on your search input.");
                
                int nLoop = 0;
                gogn = new BylisHrutur[nFjoldi];
                
                while(rsGogn.next() && nLoop <= nFjoldi)
                {           
                   
                //public Sheep(int FVSeqNumber, int color,String fatherName, String mother, String father, String name)
                
                    BylisHrutur Hrutur = new BylisHrutur(
                        rsGogn.getInt("busnumer"), 
                        rsGogn.getString("valnumer"), 
                        rsGogn.getString("ormerki"), 
                        rsGogn.getInt("hrutur_numer"), 
                        rsGogn.getString("nafn"), 
                        rsGogn.getInt("litur"),
                        rsGogn.getInt("blup_fita"), 
                        rsGogn.getInt("blup_gerd"),
                        rsGogn.getInt("blup1_mjolkurlagni"),
                        rsGogn.getInt("blup1_frjosemi"),
                        rsGogn.getInt("blup2_mjolkurlagni"),
                        rsGogn.getInt("blup2_frjosemi"),
                        rsGogn.getInt("blup3_mjolkurlagni"),
                        rsGogn.getInt("blup3_frjosemi"),
                        rsGogn.getInt("blup4_mjolkurlagni"),
                        rsGogn.getInt("blup4_frjosemi"),
                        rsGogn.getInt("blup_einkunn_m"),
                        rsGogn.getInt("blup_einkunn_f"),
                        rsGogn.getString("valnumer_ff"),
                        rsGogn.getString("ormerki_ff"),
                        rsGogn.getInt("numer_ff"),
                        rsGogn.getString("nafn_ff"),
                        rsGogn.getInt("litur_ff"),
                        rsGogn.getString("valnumer_fff"),
                        rsGogn.getString("ormerki_fff"),
                        rsGogn.getInt("numer_fff"),
                        rsGogn.getString("nafn_fff"),
                        rsGogn.getInt("litur_fff"),
                        rsGogn.getString("valnumer_mm"),
                        rsGogn.getString("ormerki_mm"),
                        rsGogn.getInt("numer_mm"),
                        rsGogn.getString("nafn_mm"),
                        rsGogn.getInt("litur_mm"),
                        rsGogn.getString("valnumer_mmm"),
                        rsGogn.getString("ormerki_mmm"),
                        rsGogn.getInt("numer_mmm"),
                        rsGogn.getString("nafn_mmm"),
                        rsGogn.getInt("litur_mmm")
                        );
                        
                      gogn[nLoop] = Hrutur;  
        //                gogn[nLoop] = new BylisHrutur(Hrutur);
        /*
                    gogn[nLoop] = new RamDataOut(
                        rsGogn.getInt("busnumer"),
                        Hrutur);
        */
                    nLoop++;
                }
            }
            catch (SQLException e) 
            {
                throw new DataNotFoundException("Could not find any data based on your search input.");
            }
    
            try 
            {
                conn.close();
                databaseConnection.close();
            } 
            catch (SQLException e) 
            {
                throw new DataNotFoundException("Could not close databaseconnection: " + 
                                    e.getMessage());
                //gogn.setWarning("Could not close databaseconnection: " + 
                  //                e.getMessage());
    
            }
        }
        finally 
        {
            gangaFraFinally(rsGogn, pstmt, conn);    
        }
        return gogn;
    }         

    @WebMethod
        public Fang[] getMate(int userID, String clientID, int systemID, int motherID) 
                                            throws AuthenticationException, 
                     DatabaseException, 
                     DataNotFoundException, 
                     UnknownException
        {
            Connection conn = null;
            ResultSet rsGogn = null;
            PreparedStatement pstmt = null;
            Fang[] fong = null;
            
            DatabaseConnection databaseConnection = 
            /*Qurinus*///new DatabaseConnection("jdbc/bondiDS");
            /*Pan*///new DatabaseConnection("jdbc/S2000_PanDS");
            new DatabaseConnection(getDatabaseName());
           
           try
           {
                try 
                {
                    conn = databaseConnection.getConnection();
                } 
                catch (SQLException e) 
                {
                    ByteArrayOutputStream os = new ByteArrayOutputStream();
                    e.printStackTrace(new PrintStream(os));
                    throw new DatabaseException(e.getClass().getName(), 
                                                os.toString(), 
                                                "Error while trying to get SQL Connection from DataSource.");
                } 
                catch (NamingException e) 
                {
                    ByteArrayOutputStream os = new ByteArrayOutputStream();
                    e.printStackTrace(new PrintStream(os));
                    throw new DatabaseException(e.getClass().getName(), 
                                                        os.toString(), 
                                                    "Error while trying to locate DataSource.");
                }
                
                //Identification
                try
                {     
                    if(!getAuthentication(conn, userID, clientID, systemID))
                    {
                        throw new AuthenticationException("Username, password and system tuple is invalid.");
                    }
                }
                catch(SQLException e)
                {
                    e.printStackTrace();
                }
                
                //skrifa logg
                writeLog(conn, "getFang", clientID, null, null, null);
                
                //get the data
                
                int nFjoldi = 0;
                
                String sSQL = "";
                int nThrep = 0;
                
                try 
                {    
                    /* ekki til neitt view sem heitir v_opid_framleidsluar og d�lkar heita anna� � v_ofeigur_fang
                     
                    sSQL =  "SELECT f.numer fang_numer, f.modir modir_numer, f.fadir fadir_numer, " +
                            "TO_CHAR(f.dagsetning, 'dd.mm.rrrr') dagsetning, vop.artal fangar " + 
                            "from lamb.v_ofeigur_fang f "+ 
                            "join einstaklingur e on f.modir = e.numer " +
                            "join v_opid_framleidsluar vop " +
                            "  on e.byli_numer = vop.byli_numer and f.framleidsluar_numer = vop.ar_numer " +
                            "where f.modir = " + motherID ;
                    */
                    
                    //���
                    sSQL = "SELECT f.fang_numer, f.modir_numer, f.fadir_numer, " + 
                           "TO_CHAR(f.dagsetning, 'dd.mm.rrrr') dagsetning, vop.ar fangar " + 
                           "from lamb.v_ofeigur_fang f " + 
                           "join gripur e " + 
                           "on f.modir_numer = e.numer " + 
                           "join framleidsluar vop " + 
                           "on e.bu_numer = vop.bu_numer " + 
                           "and f.fangar = vop.ar " + 
                           "where f.modir_numer = ?";
                                            
                    pstmt = conn.prepareStatement(
                        sSQL,
                        java.sql.ResultSet.TYPE_SCROLL_SENSITIVE, 
                        java.sql.ResultSet.CONCUR_READ_ONLY);
                        
                    pstmt.setInt(1, motherID);
                        
                    rsGogn = pstmt.executeQuery();
                    
                    //get record count
                    rsGogn.last();
                    nFjoldi = rsGogn.getRow();
                    rsGogn.beforeFirst();           
                } 
                catch (SQLException e) 
                {
                    ByteArrayOutputStream os = new ByteArrayOutputStream();
                    e.printStackTrace(new PrintStream(os));
                    throw new DatabaseException(e.getClass().getName(), 
                                                os.toString(), 
                                                "Error while preparing search statement.");
                }
    
                try 
                {  
                    if(nFjoldi == 0)
                        throw new DataNotFoundException("1. Could not find any data based on your search input.");
                    nThrep = 1;
                    int nLoop = 0;
                    fong = new Fang[nFjoldi];
                    nThrep = 2;
                    while(rsGogn.next() && nLoop <= nFjoldi)
                    {   
                        fong[nLoop] = new Fang(
                            rsGogn.getInt("fang_numer"),
                            rsGogn.getInt("modir_numer"),
                            rsGogn.getInt("fadir_numer"),        
                            rsGogn.getString("dagsetning"),
                            rsGogn.getString("fangar")   
                            );     
                        nLoop++;
                    }
                  nThrep = 3;  
                }
                catch (SQLException e) 
                {
                    //System.out.println(e);
                    throw new DataNotFoundException("2. Could not find any data based on your search input. "+nThrep);
                }
    
                try 
                {
                    databaseConnection.close();
                } 
                catch (SQLException e) 
                {
                    throw new DataNotFoundException("Could not close databaseconnection: " + 
                                        e.getMessage());
                    //gogn.setWarning("Could not close databaseconnection: " + 
                      //                e.getMessage());
    
                }
           }
           finally 
           {
                gangaFraFinally(rsGogn, pstmt, conn);    
           }
            return fong;
    }   
        
        
    //----------------------------------------------------------------------------    

    @WebMethod
    public RFIDIn pushMate( int userID, String clientID, int systemID,
                             int nFangID,
                             int nMotherID, int nFatherID, String Date,
                             int nDelete )
                                         throws AuthenticationException, 
                                                DatabaseException, 
                                                DataNotFoundException, 
                                                UnknownException 
    {

        RFIDIn inn = new RFIDIn();
        Connection conn = null;
        CallableStatement cstmt = null;
        
        DatabaseConnection databaseConnection = 
        /*Qurinus*///new DatabaseConnection("jdbc/bondiDS");
        /*Pan*///new DatabaseConnection("jdbc/S2000_PanDS");
        new DatabaseConnection(getDatabaseName());
    
        try
        {    
            try 
            {
                conn = databaseConnection.getConnection();
            } 
            catch (SQLException e) 
            {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                e.printStackTrace(new PrintStream(os));
                throw new DatabaseException(e.getClass().getName(), 
                                            os.toString(), 
                                            "Error while trying to get SQL Connection from DataSource.");
            }
            catch (NamingException e) 
            {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                e.printStackTrace(new PrintStream(os));
                throw new DatabaseException(e.getClass().getName(), 
                                            os.toString(), 
                                            "Error while trying to locate DataSource.");
            }
            
            //au�kenning
            try
            {
                if (!getAuthentication(conn, userID, clientID, systemID)) 
                {
                    throw new AuthenticationException("Username, password and system tuple is invalid.");
                }
            }
            catch(SQLException e)
            {
                e.printStackTrace();
            }
            
            //skrifa logg
            writeLog(conn, "pushMate", clientID, null, null, null);    
    
            int nVilla; 
            try 
            {
                cstmt = 
                    conn.prepareCall("{ call pk_Ofeigur.Skra_Fang(?,?,?,?,?,?,?)}");
        
                cstmt.setInt(1, nFangID);
                cstmt.setInt(2, nMotherID);
                cstmt.setInt(3, nFatherID);
                cstmt.setString(4, Date);
                cstmt.setInt(5, nDelete);
                cstmt.setInt(6, userID);
                cstmt.registerOutParameter(7, Types.INTEGER);
                
                cstmt.execute();
                nVilla = cstmt.getInt(7);
                
                if(nVilla != 0)
                 {//me?h?ndlun villu, sett inn 04.10.2010, IAR.
                     String sVilla = "";
                     CallableStatement cstmtVilla = 
                     conn.prepareCall("{ call ? := fn_get_villa(?)}");
                     cstmtVilla.registerOutParameter(1, Types.VARCHAR);
                     cstmtVilla.setInt(2, nVilla);
                     cstmtVilla.execute();
                     sVilla = cstmtVilla.getString(1);
                     cstmtVilla.close();
                     //System.out.println(nVilla);
    
                    inn.setWarning(sVilla);
                 }
    
                inn.setResult(nVilla);
                
                if(nVilla == 0)
                    conn.commit();
                else
                    conn.rollback();
            } 
            catch (SQLException e) 
            {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                e.printStackTrace(new PrintStream(os));
                throw new DatabaseException(e.getClass().getName(), 
                                            os.toString(), 
                                            "Error while preparing update call.");
            }   
            /*catch(DatabaseException ex){
                ex.getMessage();
            }*/
            try 
            {
                databaseConnection.close();
            } 
            catch (SQLException e) 
            {
                inn.setWarning("Could not close databaseconnection: " + 
                                       e.getMessage());
            }
            
        }
        finally 
        {
            gangaFraFinally(cstmt, conn);    
        }
        
        return inn;
    }        
/* 
    public boolean getConnection(Connection conn, String sLogText, String sClID, String IPAddress, String MacAddress, String ClVersion)
    {   
        DatabaseConnection databaseConnection = new DatabaseConnection(getDatabaseName());
        
        try 
        {
            conn = databaseConnection.getConnection();
        } 
        catch (SQLException e) 
        {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(os));
            throw new DatabaseException(e.getClass().getName(), 
                                        os.toString(), 
                                        "Error while trying to get SQL Connection from DataSource.");
        }
        catch (NamingException e) 
        {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(os));
            throw new DatabaseException(e.getClass().getName(), 
                                        os.toString(), 
                                        "Error while trying to locate DataSource.");
        }
        //Au?kenning
        try
        {
            
            if(!getAuthentication(conn, userID, clientID, systemID))
            {
                throw new AuthenticationException("Username, password and system tuple is invalid.");
            }
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        } 
    }
*/    
  @WebMethod
  public Domari[] getIudex(int userID, String clientID, int systemID) 
                                           throws 
                                               AuthenticationException,
                                               DatabaseException, 
                                               DataNotFoundException, 
                                               UnknownException
  {   
      Connection conn = null;
      ResultSet rsGogn = null;
      Domari[] gogn = null;
      PreparedStatement pstmt = null;
      
      DatabaseConnection databaseConnection = new DatabaseConnection(getDatabaseName());
      
      try
      {
          try 
          {
              conn = databaseConnection.getConnection();
          } 
          catch (SQLException e) 
          {
              ByteArrayOutputStream os = new ByteArrayOutputStream();
              e.printStackTrace(new PrintStream(os));
              throw new DatabaseException(e.getClass().getName(), 
                                          os.toString(), 
                                          "Error while trying to get SQL Connection from DataSource.");
          }
          catch (NamingException e) 
          {
              ByteArrayOutputStream os = new ByteArrayOutputStream();
              e.printStackTrace(new PrintStream(os));
              throw new DatabaseException(e.getClass().getName(), 
                                          os.toString(), 
                                          "Error while trying to locate DataSource.");
          }
          //Au?kenning
          try
          {
              
              if(!getAuthentication(conn, userID, clientID, systemID))
              {
                  throw new AuthenticationException("Username, password and system tuple is invalid.");
              }
          }
          catch(SQLException e)
          {
              e.printStackTrace();
          }
    
          //skrifa logg
          //  IP hva?a?ra h????? - athuga seinna
          writeLog(conn, "getIudex", clientID, null, null, null);
    
          //S?kja g?gn
                
          int nFjoldi = 0;
          
          String sSQL;
          
          try 
          {     
              sSQL = " select d.numer, p.kennitala, p.fullt_nafn, d.netfang, d.simi " + 
                     " from domari d, kjarni.persona p " + 
                     " where d.persona_numer = p.numer ";
    
              pstmt = conn.prepareStatement(
                  sSQL,
                  java.sql.ResultSet.TYPE_SCROLL_SENSITIVE, 
                  java.sql.ResultSet.CONCUR_READ_ONLY);
                  
              rsGogn = pstmt.executeQuery();
              
              //fj?lda f?rslna
              rsGogn.last();
              nFjoldi = rsGogn.getRow();
              rsGogn.beforeFirst();
              
          } 
          catch (SQLException e) 
          {
              ByteArrayOutputStream os = new ByteArrayOutputStream();
              e.printStackTrace(new PrintStream(os));
              throw new DatabaseException(e.getClass().getName(), 
                                          os.toString(), 
                                          "Error while preparing search statement.");
          }
    
          try 
          {  
              if(nFjoldi == 0)
                  throw new DataNotFoundException("Could not find any data based on your search input.");
              
              int nLoop = 0;
              gogn = new Domari[nFjoldi];
              
              while(rsGogn.next() && nLoop <= nFjoldi)
              {  
              
                  Domari d = new Domari(
                      rsGogn.getInt("numer"), 
                      rsGogn.getString("kennitala"), 
                      rsGogn.getString("fullt_nafn"), 
                      rsGogn.getString("netfang"),                   
                      rsGogn.getString("simi")                  
                      );
                      
                    gogn[nLoop] = d;    
      
                  nLoop++;
              }
          }
          catch (SQLException e) 
          {
              throw new DataNotFoundException("Could not find any data based on your search input.");
          }
    
          try 
          {
              conn.close();
              //databaseConnection.close();
          } 
          catch (SQLException e) 
          {
              throw new DataNotFoundException("Could not close databaseconnection: " + 
                                  e.getMessage());
              //gogn.setWarning("Could not close databaseconnection: " + 
                //                e.getMessage());
    
          }
     }
     finally 
     {
         gangaFraFinally(rsGogn, pstmt, conn);    
     }
      return gogn;
  }         
  
  //------------------------------------------------------------------------------------
  
  @WebMethod
  public AfkvrOut[] getAfkvr(int userID, String clientID, int systemID, int animalID) 
                                           throws 
                                               AuthenticationException,
                                               DatabaseException, 
                                               DataNotFoundException, 
                                               UnknownException
  {   
    Connection conn = null;
    PreparedStatement pstmt = null;
    PreparedStatement pstmtTh = null;
    ResultSet rsGogn = null;
    ResultSet rsGognTh = null;
    AfkvrOut[] gogn = null;
    Thungi [] gognTh = null;
      
    DatabaseConnection databaseConnection = 
    /*Qurinus*///new DatabaseConnection("jdbc/bondiDS");
    /*Pan*///new DatabaseConnection("jdbc/S2000_PanDS");
    new DatabaseConnection(getDatabaseName());

    try
    {
        try {
            conn = databaseConnection.getConnection();
        } 
        
        catch (SQLException e) {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(os));
            throw new DatabaseException(e.getClass().getName(), os.toString(), 
                                        "Error while trying to get SQL Connection from DataSource.");
        } 
        
        catch (NamingException e) {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(os));
            throw new DatabaseException(e.getClass().getName(), os.toString(), 
                                        "Error while trying to locate DataSource.");
        }
        //Au�kenning
        try {
    
            if (!getAuthentication(conn, userID, clientID, systemID)) {
                throw new AuthenticationException("Username, password and system tuple is invalid.");
            }
        } 
        
        catch (SQLException e) {
            e.printStackTrace();
        }
    
        //skrifa logg
        writeLog(conn, "getAfkvri", clientID, null, null, null);
    
    
        //S�kja g�gn
    
        
        int nFjoldi = 0;
        int nFjoldiTh;
        
        String sSQL;
        String sSQLTh;
        //int nEinstaklingur;
        
        try {
            sSQL = 
                    "select " + 
                          "einstaklingur_numer , " + 
                          "dagsetning , " + 
                          "taeki , " + 
                          "domari , " + 
                          "maelingamadur , " + 
                          "lambanumer , " + 
                          "ormerki , " + 
                          //"TO_CHAR(faedingardags, 'dd.mm.rrrr') as faedingardags , " + 
                          "faedingardags , " + 
                          "kyn , " + 
                          "litur , " + 
                          "burdur , " + 
                          "gengin , " + 
                          "fotleggur , " + 
                          "omvodvi , " + 
                          "omfita , " + 
                          "logun_vodva , " + 
                          "haus , " + 
                          "halsherdar , " + 
                          "bringautlogur , " + 
                          "bak , " + 
                          "malir , " + 
                          "laeri , " + 
                          "ull , " + 
                          "faetur , " + 
                          "samraemi , " + 
                          "alls , " + 
                          "domur_athugasemd , " + 
                          "fadir_valnr , " + 
                          "fadir_nafn , " + 
                          "modir_valnr , " + 
                          "modir_nafn, " + 
                          "blup_fita , " + 
                          "blup_gerd , " + 
                          "blup_mjolk , " + 
                          "blup_frjosemi  " + 
                          "from v_ofeigur_afkvr " + 
                          //"where einstaklingur_numer = " + animalID;
                          "where einstaklingur_numer = ?"; //���
                          
             pstmt = conn.prepareStatement(sSQL, java.sql.ResultSet.TYPE_SCROLL_SENSITIVE, 
                                      java.sql.ResultSet.CONCUR_READ_ONLY);
                                      
            pstmt.setInt(1, animalID); //���
    
            rsGogn = pstmt.executeQuery();
    
            //fj�lda f�rslna
            rsGogn.last();
            nFjoldi = rsGogn.getRow();
            rsGogn.beforeFirst();
            
        //            pstmt.close();
    
        } 
        
        catch (SQLException e) {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(os));
            throw new DatabaseException(e.getClass().getName(), os.toString(), 
                                        "Error while preparing search statement.");
        }
    
        try {
            if (nFjoldi == 0)
                throw new DataNotFoundException("Could not find any data based on your search input.");
    
            int nLoop = 0;
            gogn = new AfkvrOut[nFjoldi];
            
            while(rsGogn.next() && nLoop <= nFjoldi)
            {
    
                //nEinstaklingur = rsGogn.getInt("einstaklingur_numer");
                
                nFjoldiTh = 0;
                gognTh = null;
                rsGognTh = null;
                
                try {
                    sSQLTh = " select TO_CHAR(t.DAGSETNING, 'dd.mm.rrrr') as DAGSETNING, " + 
                    "                  t.THUNGIKG, t.FAEDINGARTHUNGI " + 
                    "                  from V_OFEIGUR_THUNGI t " + 
                    //"                  where t.EINSTAKLINGUR_NUMER = " + animalID +
                    "                  where t.EINSTAKLINGUR_NUMER = ? " +
                    "                  order by t.dagsetning ";
                    
                    
                    pstmtTh = conn.prepareStatement(sSQLTh, java.sql.ResultSet.TYPE_SCROLL_SENSITIVE, 
                                              java.sql.ResultSet.CONCUR_READ_ONLY);
                                              
                    pstmtTh.setInt(1, animalID); //���
    
                    rsGognTh = pstmtTh.executeQuery();
        //                    pstmtTh.close();
    
                    //fj�lda f�rslna
                    rsGognTh.last();
                    nFjoldiTh = rsGognTh.getRow();
                    rsGognTh.beforeFirst();
                    
                    } 
                    catch (SQLException e) {
                        ByteArrayOutputStream os = new ByteArrayOutputStream();
                        e.printStackTrace(new PrintStream(os));
                        throw new DatabaseException(e.getClass().getName(), os.toString(), 
                                                    "Error while preparing search statement.");
                    }                    
    
                    if (nFjoldiTh > 0) {                    
    
                        try {
                        
                             int nLoopTh = 0;
                            gognTh = new Thungi [nFjoldiTh];
                        
                            while(rsGognTh.next() && nLoopTh <= nFjoldiTh)
                            {
    
                            Thungi EinstThungi = new Thungi (
                                rsGognTh.getString("dagsetning"),
                                rsGognTh.getString("thungikg"),
                                rsGognTh.getString("faedingarthungi")
                            );
    
                            gognTh[nLoopTh] = EinstThungi;
                            
                            nLoopTh ++;
                            }                   
                        }  
                    
                        catch (SQLException e)
                        {
                        throw new DataNotFoundException("Could not find any data based on your search input.");
                        }
                    }
                 
                    AfkvrOut afkvr = new AfkvrOut(
                       rsGogn.getString("dagsetning"),
                       rsGogn.getInt("taeki"),
                       rsGogn.getInt("domari"),
                       rsGogn.getInt("maelingamadur"),
                       rsGogn.getString("lambanumer"),
                       rsGogn.getString("ormerki"),
                       rsGogn.getString("faedingardags"),                   
                       rsGogn.getInt("kyn"),
                       rsGogn.getInt("litur"),
                       rsGogn.getInt("burdur"),
                       rsGogn.getInt("gengin"),
                       rsGogn.getInt("fotleggur"),
                       rsGogn.getString("omvodvi"),
                       rsGogn.getString("omfita"),
                       rsGogn.getString("logun_vodva"),
                       rsGogn.getString("haus"),
                       rsGogn.getString("halsherdar"),
                       rsGogn.getString("bringautlogur"),
                       rsGogn.getString("bak"),
                       rsGogn.getString("malir"),
                       rsGogn.getString("laeri"), 
                       rsGogn.getString("ull"),
                       rsGogn.getString("faetur"), 
                       rsGogn.getString("samraemi"), 
                       rsGogn.getString("alls"), 
                       rsGogn.getString("domur_athugasemd"), 
                       rsGogn.getString("fadir_valnr"),
                       rsGogn.getString("fadir_nafn"),
                       rsGogn.getString("modir_valnr"), 
                       rsGogn.getString("modir_nafn"),
                       rsGogn.getString("blup_fita"),
                       rsGogn.getString("blup_gerd"),
                       rsGogn.getString("blup_mjolk"),
                       rsGogn.getString("blup_frjosemi"),
                       gognTh
                    );
                     
                    gogn[nLoop] = afkvr;  
                  
                if (pstmtTh != null)
                {
                    pstmtTh.close();
                    pstmtTh = null;
                }
                
                if (rsGognTh != null)
                { 
                    rsGognTh.close();
                    rsGognTh = null;
                }
                
                 nLoop++;                
    
             }
        } 
            catch (SQLException e)
            {
                throw new DataNotFoundException("Could not find any data based on your search input.");
            }
        
    
            try
            {
                if(pstmt != null)
                {
                    pstmt.close();
                    pstmt = null;
                }
                
                if(pstmtTh != null)
                {
                    pstmtTh.close();
                    pstmtTh = null;
                }
                
                if(rsGogn != null) {
                    rsGogn.close();
                    rsGogn = null;
                }
                
                if(rsGognTh != null) {
                    rsGognTh.close();
                    rsGognTh = null;
                }
                
                if(conn != null) {
                    conn.close();
                    conn = null;
                }
            
                
            //databaseConnection.close();
            }
            catch (SQLException e)
            {
            throw new DataNotFoundException("Could not close databaseconnection: " + 
                                e.getMessage());
            //gogn.setWarning("Could not close databaseconnection: " + 
              //                e.getMessage());
    
            }
        }
        finally 
        {
            gangaFraFinally(rsGogn, pstmt, conn);
            gangaFraFinally(rsGognTh, pstmtTh, null);
        }
        return gogn;
  }         
  
  //------------------------------------------------------------------------------
  
  @WebMethod
  public RFIDIn pushAfkvr( int userID, String clientID, int systemID,
                           int animalID, AfkvrIn Afkvr)
                                       throws AuthenticationException, 
                                              DatabaseException, 
                                              DataNotFoundException, 
                                              UnknownException 
  {

      RFIDIn inn = new RFIDIn();
      Connection conn = null;
      CallableStatement cstmt = null;
      
      DatabaseConnection databaseConnection = 
      /*Qurinus*///new DatabaseConnection("jdbc/bondiDS");
      /*Pan*///new DatabaseConnection("jdbc/S2000_PanDS");
      new DatabaseConnection(getDatabaseName());
      
      try
      {
          try 
          {
              conn = databaseConnection.getConnection();
          } 
          catch (SQLException e) 
          {
              ByteArrayOutputStream os = new ByteArrayOutputStream();
              e.printStackTrace(new PrintStream(os));
              throw new DatabaseException(e.getClass().getName(), 
                                          os.toString(), 
                                          "Error while trying to get SQL Connection from DataSource.");
          }
          catch (NamingException e) 
          {
              ByteArrayOutputStream os = new ByteArrayOutputStream();
              e.printStackTrace(new PrintStream(os));
              throw new DatabaseException(e.getClass().getName(), 
                                          os.toString(), 
                                          "Error while trying to locate DataSource.");
          }
          
          //au�kenning
          try
          {
              if (!getAuthentication(conn, userID, clientID, systemID)) 
              {
                  throw new AuthenticationException("Username, password and system tuple is invalid.");
              }
          }
          catch(SQLException e)
          {
              e.printStackTrace();
          }
          
          //skrifa logg
          writeLog(conn, "pushAfkvr", clientID, null, null, null);    
    
          int nVilla = 0; 
          String sVilla = "";
          try 
          {
              cstmt = 
                  conn.prepareCall("{ call pk_Ofeigur.Skra_Afkvr(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
      
              cstmt.setInt(1, userID);
              cstmt.setInt(2, animalID);
              cstmt.setString(3, Afkvr.getDagsetning());
              cstmt.setInt(4, Afkvr.getTaeki());
              cstmt.setInt(5, Afkvr.getDomari());
              cstmt.setInt(6, Afkvr.getMaelingaMadur());
              cstmt.setInt(7, Afkvr.getKyn());
              cstmt.setString(8, Afkvr.getLifthungi());
              cstmt.setInt(9, Afkvr.getFotleggur());
              cstmt.setString(10, Afkvr.getOmvodvi());
              cstmt.setString(11, Afkvr.getOmfita());
              cstmt.setString(12, Afkvr.getLogun());
              cstmt.setString(13, Afkvr.getHaus());
              cstmt.setString(14, Afkvr.getHalsHerdar());
              cstmt.setString(15, Afkvr.getBringaUtlogur());
              cstmt.setString(16, Afkvr.getBak());
              cstmt.setString(17, Afkvr.getMalir());
              cstmt.setString(18, Afkvr.getLaeri());
              cstmt.setString(19, Afkvr.getUll());
              cstmt.setString(20, Afkvr.getFaetur());
              cstmt.setString(21, Afkvr.getSamraemi());
              cstmt.setString(22, Afkvr.getAlls());
              cstmt.setString(23, Afkvr.getAthugasemdir());
              
              cstmt.registerOutParameter(24, Types.INTEGER);
              cstmt.registerOutParameter(25, Types.VARCHAR);
              
              cstmt.execute();
              nVilla = cstmt.getInt(24);
              sVilla = cstmt.getString(25);
              
              if(nVilla != 0)
               {//me?h?ndlun villu, sett inn 04.10.2010, IAR.
                   //String sVilla = "";
                   CallableStatement cstmtVilla = 
                   conn.prepareCall("{ call ? := fn_get_villa(?)}");
                   cstmtVilla.registerOutParameter(1, Types.VARCHAR);
                   cstmtVilla.setInt(2, nVilla);
                   cstmtVilla.execute();
                   sVilla = cstmtVilla.getString(1);
                   cstmtVilla.close();
                   //System.out.println(nVilla);
    
                  inn.setWarning(sVilla);
               }
    
              inn.setResult(nVilla);
              
              if(nVilla == 0)
                  conn.commit();
              else
                  conn.rollback();
          } 
          catch (SQLException e) 
          {
              ByteArrayOutputStream os = new ByteArrayOutputStream();
              e.printStackTrace(new PrintStream(os));
              throw new DatabaseException(e.getClass().getName(), 
                                          os.toString(), 
                                          "Error while preparing update call.");
          }   
          catch(DatabaseException ex){
              ex.getMessage();
          }
          try 
          {
              databaseConnection.close();
          } 
          catch (SQLException e) 
          {
              inn.setWarning("Could not close databaseconnection: " + 
                                     e.getMessage());
          }
      }
      finally 
      {
            gangaFraFinally(cstmt, conn);    
      }
      return inn;
  }        
  
    private void gangaFraFinally(ResultSet rs, PreparedStatement pstmt, Connection con) 
    {
        try
        {
            if(rs != null)
            {
                rs.close();
                rs = null;
            }
        }
        catch (SQLException e) 
        {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(os));
        }
        
        try
        {
            if(pstmt != null)
            { 
                pstmt.close();
                pstmt = null;
            }
            
        }
        catch (SQLException e) 
        {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(os));
        }
        
        try
        {
            if(con != null)
            {
                con.close();
                con = null;
            }
        }
        catch (SQLException e) 
        {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(os));
        }
    }

    private void gangaFraFinally(CallableStatement pstmt, Connection con) 
    {
        try
        {
            if(pstmt != null)
            { 
                pstmt.close();
                pstmt = null;
            }
            
        }
        catch (SQLException e) 
        {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(os));
        }
        
        try
        {
            if(con != null)
            {
                con.close();
                con = null;
            }
        }
        catch (SQLException e) 
        {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(os));
        }
    }

    private int getProductionYear(Connection conn, int nFarmId) throws SQLException
    {
      String sSQL = "";
      PreparedStatement pStmt = null;
      ResultSet rsResult = null;
      int nFramleidsluar = 0;
      
      try 
      {
        sSQL =  "select ar from framleidsluar where bu_numer = ?";
      
        pStmt = conn.prepareStatement(sSQL);
        pStmt.setInt(1, nFarmId);
        
        rsResult= pStmt.executeQuery();
        if(rsResult.next())
          nFramleidsluar  = rsResult.getInt("AR");
      
        pStmt.close();
        rsResult.close();
        pStmt = null;
        rsResult = null;    
      } 
      catch (Exception e) 
      {
        e.printStackTrace();
        throw new SQLException("Error while getting production year");
      }
      finally
      {
        if(rsResult != null)
        {
          rsResult.close();
          rsResult = null;
        }   
        if(pStmt != null)
        { 
          pStmt.close();
          pStmt = null;
        }
      }
      return nFramleidsluar;      
    }

}
