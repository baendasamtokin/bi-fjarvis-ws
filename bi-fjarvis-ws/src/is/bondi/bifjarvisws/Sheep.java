package is.bondi.bifjarvisws;

import java.io.Serializable;

public class Sheep implements Serializable {

    private int FVSeqNumber;
    private String VID;         //gripan�mer
    private String RFID;        //�rmerki
    private String name;        //nafn
    private int color;          //litur - litan�mer
    private String mother;      //m��ir gripan�mer
    private String motherName;  //m��ir nafn
    private String father;      //fa�ir gripan�mer
    private String fatherName;  //fa�ir nafn
   
    public Sheep() 
    {
    }
    
    public Sheep(int FVSeqNumber, int color, String VID, String RFID, String motherName, String fatherName, String mother, String father, String name)
    {
        this.FVSeqNumber = FVSeqNumber;
        this.color = color;
        this.VID = VID;
        this.RFID = RFID;
        this.motherName = motherName;
        this.fatherName = fatherName;
        this.mother = mother;
        this.father = father;
        this.name = name;
    }

    public int getFVSeqNumber() {
       return FVSeqNumber;
    }
    
    public void setFVSeqNumber(int FVSeqNumber) {
       this.FVSeqNumber = FVSeqNumber;
    }
    
    public int getColor() {
       return color;
    }
    
    public void setColor(int color) {
       this.color = color;
    }
    
    public String getVID() {
       return VID;
    }
    
    public void setVID(String VID) {
       this.VID = VID;
    }
    
    public String getRFID() {
       return RFID;
    }
    
    public void setRFID(String RFID) {
       this.RFID = RFID;
    }
    
    public String getMotherName() {
       return motherName;
    }
    
    public void setMotherName(String motherName) {
       this.motherName = motherName;
    }
    
    public String getFatherName() {
       return fatherName;
    }
    
    public void setFatherName(String fatherName) {
       this.fatherName = fatherName;
    }
    
    public String getFather() {
       return father;
    }
    
    public void setFather(String father) {
       this.father = father;
    }
    
    public String getName() {
       return name;
    }
    
    public void setName(String name) {
       this.name = name;
    }
    
    public String getMother() {
       return mother;
    }
    
    public void setMother(String mother) {
       this.mother = mother;
    }
    }