 package is.bondi.bifjarvisws;

 import java.io.Serializable;

 public class AuthenticationException extends Exception implements Serializable {

     // H�r hef �g eing�ngu message til a� h�gt s� a� segja �eim sem tengist a� au�kennisuppl�singarnar
     // sem hann sendi hafi veri� rangar. Af �ryggis�st��um er alltaf gott a� segja sem minnst um hva�
     // f�r �rskei�is vi� aukenninguna, �v� t�lvu�rj�tar geta oft notf�rt s�r sl�kar uppl�singar vi� a�
     // skipuleggja frekari �r�sir � kerfi�.
     private String message = null;

     public AuthenticationException() {
     }
     
     public AuthenticationException(String message) {
         this.message = message; 
     }

     public void setMessage(String message) {
         this.message = message;
     }

     public String get_message() {
         return message;
     }
 }
