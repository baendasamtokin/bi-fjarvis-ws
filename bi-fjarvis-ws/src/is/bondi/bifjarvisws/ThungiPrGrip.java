package is.bondi.bifjarvisws;

import java.io.Serializable;

public class ThungiPrGrip implements Serializable {
    private int einstaklingurNumer;
    private String dagsetning;
    private String thungiKg;
    private String faedingarthungi;
    
    public ThungiPrGrip() {
    }

    public ThungiPrGrip (int einstaklingurNumer, String dagsetning, 
                         String thungiKg, String faedingarthungi) {
    this.einstaklingurNumer = einstaklingurNumer; 
    this.dagsetning = dagsetning;
    this.thungiKg = thungiKg;
    this.faedingarthungi = faedingarthungi;
    }
                         

    public void setEinstaklingurNumer(int einstaklingurNumer) {
        this.einstaklingurNumer = einstaklingurNumer;
    }

    public int getEinstaklingurNumer() {
        return einstaklingurNumer;
    }

    public void setDagsetning(String dagsetning) {
        this.dagsetning = dagsetning;
    }

    public String getDagsetning() {
        return dagsetning;
    }

    public void setThungiKg(String thungiKg) {
        this.thungiKg = thungiKg;
    }

    public String getThungiKg() {
        return thungiKg;
    }

    public void setFaedingarthungi(String faedingarthungi) {
        this.faedingarthungi = faedingarthungi;
    }

    public String getFaedingarthungi() {
        return faedingarthungi;
    }
}
