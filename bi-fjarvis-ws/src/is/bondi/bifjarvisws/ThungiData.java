package is.bondi.bifjarvisws;

import java.io.Serializable;

public class ThungiData implements Serializable {
    //private int busnumer;
    //private int einstaklingurNumer;
    //private String valnr;
    private String lambanumer;
    //private String ormerki;
    private String nafn;
    private int litanumer;
    //private int kyn;
    private String faedingardags;
    private int fadirNumer;
    private String fadirValnr;
    private String fadirNafn;
    private int fadirLitur;
    private int modirNumer;
    private String modirValnr;
    private String modirNafn;
    private int modirLitur;
    private int blupFita;
    private int blupGerd;
    private int blupMjolk;
    private int blupFrjosemi;
    private int burdur;
    private Thungi [] thungi;
    
    public ThungiData() {
    }
    
    public ThungiData (/*int busnumer, int einstaklingurNumer, String valnr,*/
                       String lambanumer, /*String ormerki,*/ String nafn,
                       int litanumer, /*int kyn,*/ String faedingardags,
                       int fadirNumer, String fadirValnr, String fadirNafn,
                       int fadirLitur, int modirNumer, String modirValnr,
                       String modirNafn, int modirLitur, int blupFita,
                       int blupGerd, int blupMjolk, int blupFrjosemi, 
                       int burdur, Thungi [] thungi ){
        //this.busnumer = busnumer;
        //this.einstaklingurNumer = einstaklingurNumer;
        //this.valnr = valnr;
        this.lambanumer = lambanumer;
        //this.ormerki = ormerki;
        this.nafn = nafn;
        this.litanumer = litanumer;
        //this.kyn = kyn;
        this.faedingardags = faedingardags;
        this.fadirNumer = fadirNumer;
        this.fadirValnr = fadirValnr;
        this.fadirNafn = fadirNafn;
        this.fadirLitur = fadirLitur;
        this.modirNumer = modirNumer;
        this.modirValnr = modirValnr;
        this.modirNafn = modirNafn;
        this.modirLitur = modirLitur;
        this.blupFita = blupFita;
        this.blupGerd = blupGerd;
        this.blupMjolk = blupMjolk;
        this.blupFrjosemi = blupFrjosemi;
        this.burdur = burdur;
        this.thungi = thungi;
    }
/*
    public void setBusnumer(int busnumer) {
        this.busnumer = busnumer;
    }

    public int getBusnumer() {
        return busnumer;
    }

    public void setEinstaklingurNumer(int einstaklingurNumer) {
        this.einstaklingurNumer = einstaklingurNumer;
    }

    public int getEinstaklingurNumer() {
        return einstaklingurNumer;
    }

    public void setValnr(String valnr) {
        this.valnr = valnr;
    }

    public String getValnr() {
        return valnr;
    }
  */  
    public void setLambanumer(String lambanumer) {
        this.lambanumer = lambanumer;
    }

    public String getLambanumer() {
        return lambanumer;
    }    
/*
    public void setOrmerki(String ormerki) {
        this.ormerki = ormerki;
    }

    public String getOrmerki() {
        return ormerki;
    }
*/    
    public void setNafn(String nafn) {
        this.nafn = nafn;
    }

    public String getNafn() {
        return nafn;
    }    

    public void setLitanumer(int litanumer) {
        this.litanumer = litanumer;
    }

    public int getLitanumer() {
        return litanumer;
    }
/*
    public void setKyn(int kyn) {
        this.kyn = kyn;
    }

    public int getKyn() {
        return kyn;
    }
*/
    public void setFaedingardags(String faedingardags) {
        this.faedingardags = faedingardags;
    }

    public String getFaedingardags() {
        return faedingardags;
    }

    public void setFadirNumer(int fadirNumer) {
        this.fadirNumer = fadirNumer;
    }

    public int getFadirNumer() {
        return fadirNumer;
    }

    public void setFadirValnr(String fadirValnr) {
        this.fadirValnr = fadirValnr;
    }

    public String getFadirValnr() {
        return fadirValnr;
    }

    public void setFadirNafn(String fadirNafn) {
        this.fadirNafn = fadirNafn;
    }

    public String getFadirNafn() {
        return fadirNafn;
    }

    public void setFadirLitur(int fadirLitur) {
        this.fadirLitur = fadirLitur;
    }

    public int getFadirLitur() {
        return fadirLitur;
    }

    public void setModirNumer(int modirNumer) {
        this.modirNumer = modirNumer;
    }

    public int getModirNumer() {
        return modirNumer;
    }

    public void setModirValnr(String modirValnr) {
        this.modirValnr = modirValnr;
    }

    public String getModirValnr() {
        return modirValnr;
    }

    public void setModirNafn(String modirNafn) {
        this.modirNafn = modirNafn;
    }

    public String getModirNafn() {
        return modirNafn;
    }

    public void setModirLitur(int modirLitur) {
        this.modirLitur = modirLitur;
    }

    public int getModirLitur() {
        return modirLitur;
    }

    public void setBlupFita(int blupFita) {
        this.blupFita = blupFita;
    }

    public int getBlupFita() {
        return blupFita;
    }

    public void setBlupGerd(int blupGerd) {
        this.blupGerd = blupGerd;
    }

    public int getBlupGerd() {
        return blupGerd;
    }

    public void setBlupMjolk(int blupMjolk) {
        this.blupMjolk = blupMjolk;
    }

    public int getBlupMjolk() {
        return blupMjolk;
    }

    public void setBlupFrjosemi(int blupFrjosemi) {
        this.blupFrjosemi = blupFrjosemi;
    }

    public int getBlupFrjosemi() {
        return blupFrjosemi;
    }

    public void setThungi(Thungi[] thungi) {
        this.thungi = thungi;
    }

    public Thungi[] getThungi() {
        return thungi;
    }

    public void setBurdur(int burdur) {
        this.burdur = burdur;
    }

    public int getBurdur() {
        return burdur;
    }
}

