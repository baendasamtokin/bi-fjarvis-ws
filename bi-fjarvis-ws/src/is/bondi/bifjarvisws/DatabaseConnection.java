package is.bondi.bifjarvisws;

import javax.sql.DataSource;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class DatabaseConnection {

    private DataSource ds = null;
    private Connection conn = null;
    private String dsName = null;

    public DatabaseConnection(String dsn) {
        dsName = dsn;
    }

    public Connection getConnection() throws NamingException, SQLException {
        if (ds == null) {
            Context ic = new InitialContext();
            ds = (DataSource)ic.lookup(dsName);
        }

        if (conn == null) {
            conn = ds.getConnection();
        }
        return conn;
    }

    public void close() throws SQLException {
        if (conn != null) {
            conn.close();
            conn = null;
            ds = null;
        }

    }
}
