package is.bondi.bifjarvisws;

import java.io.Serializable;

public class SpringDataOut implements Serializable {

    private SpringData springData;
    
    public SpringDataOut() {
    }
    
    public SpringDataOut(SpringData springData)
    {
        this.springData = springData;
    }
    
    public void setSpringData(SpringData springData) {
        this.springData = springData;
    }

    public SpringData getSpringData() {
        return springData;
    }
}
