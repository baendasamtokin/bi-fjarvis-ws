package is.bondi.bifjarvisws;

import java.io.Serializable;

public class Thungi implements Serializable {

    private String dagsetning;
    private String thungiKg;
    private String faedingarthungi;
    
    public Thungi() {
    }
    
    public Thungi (String dagsetning, String thungiKg,
                   String faedingarthungi ) {
        this.dagsetning = dagsetning;
        this.thungiKg = thungiKg;
        this.faedingarthungi = faedingarthungi;
    }

    public void setDagsetning(String dagsetning) {
        this.dagsetning = dagsetning;
    }

    public String getDagsetning() {
        return dagsetning;
    }

    public void setThungiKg(String thungiKg) {
        this.thungiKg = thungiKg;
    }

    public String getThungiKg() {
        return thungiKg;
    }
    
    public void setFaedingarthungi(String faedingarthungi) {
        this.faedingarthungi = faedingarthungi;
    }

    public String getFaedingarthungi() {
        return faedingarthungi;
    }    
}
