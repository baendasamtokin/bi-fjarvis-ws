package is.bondi.bifjarvisws;

import java.io.Serializable;

public class VorAer implements Serializable 
{
    private int buNumer;      
    private int aerNumer;           
    private String ormerki;      
    private String nafn;
    private int beitarsvaediNumer;       
    private int hruturNumer;        
    private String fangDags;  
    private int afdrifFangs;
    private String burdarDags;    
    private int fjoldiIBurdi;
    private String bIJS;
    private VorLamb [] lamb;
           
    public VorAer() 
    {
    }
    
    public VorAer(int buNumer, int aerNumer, String ormerki, String nafn,
                int beitarsvaediNumer, int hruturNumer, String fangDags, 
                int afdrifFangs, String burdarDags, 
                int fjoldiIBurdi, String bIJS, VorLamb[] lamb)
    {
        this.buNumer = buNumer;
        this.aerNumer = aerNumer;
        this.ormerki = ormerki;
        this.nafn = nafn;
        this.beitarsvaediNumer = beitarsvaediNumer;
        this.hruturNumer = hruturNumer;
        this.fangDags = fangDags;
        this.afdrifFangs = afdrifFangs;
        this.burdarDags = burdarDags;
        this.fjoldiIBurdi = fjoldiIBurdi;
        this.bIJS = bIJS;
        this.lamb = lamb;
    }
        
    
    public int getBuNumer() {
        return buNumer;
    }
    
    public void setBuNumer(int buNumer) {
        this.buNumer = buNumer;
    }
    
    public int getAerNumer() {
        return aerNumer;
    }
    
    public void setAerNumer(int aerNumer) {
        this.aerNumer = aerNumer;
    }
            
    public String getOrmerki() {
        return ormerki;
    }
    
    public void setOrmerki(String ormerki) {
        this.ormerki = ormerki;
    }
    
    public String getNafn() {
        return nafn;
    }
    
    public void setNafn(String nafn) {
        this.nafn = nafn;
    }
    
        
    public int getHruturNumer() {
        return hruturNumer;
    }
    
    public void setHruturNumer(int hruturNumer) {
        this.hruturNumer = hruturNumer;
    }
      
    public String getFangDags() {
        return fangDags;
    }
    
    public void setFangDags(String fangDags) {
        this.fangDags = fangDags;
    }
    
    public int getAfdrifFangs() {
        return afdrifFangs;
    }
    
    public void setAfdrifFangs(int afdrifFangs) {
        this.afdrifFangs = afdrifFangs;
    }
    
    public void setBurdarDags(String burdarDags) {
        this.burdarDags = burdarDags;
    }

    public String getBurdarDags() {
        return burdarDags;
    }    
    
    public String getBIJS() {
        return bIJS;
    }
    
    public void setBIJS(String bIJS) {
        this.bIJS = bIJS;
    }

    public void setBeitarsvaediNumer(int beitarsvaediNumer) {
        this.beitarsvaediNumer = beitarsvaediNumer;
    }

    public int getBeitarsvaediNumer() {
        return beitarsvaediNumer;
    }

    public void setLamb(VorLamb[] lamb) {
        this.lamb = lamb;
    }

    public VorLamb[] getLamb() {
        return lamb;
    }

    public void setFjoldiIBurdi(int fjoldiIBurdi) {
        this.fjoldiIBurdi = fjoldiIBurdi;
    }

    public int getFjoldiIBurdi() {
        return fjoldiIBurdi;
    }
}
