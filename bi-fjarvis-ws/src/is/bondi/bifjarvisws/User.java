package is.bondi.bifjarvisws;

public class User 
{
    private int userID;
    private int groupID;
    private int systemID;
    private String userSSID;
    private String userName;
    private String expireDate;    
    private String ofeigurVersion;
    private String timestamp;

    public User() {
    }
    
    public User(int userID, int groupID, int systemID, String userSSID, String userName, String expireDate, String ofeigurVersion, String timestamp)
    {
        this.userID = userID;
        this.groupID = groupID;
        this.systemID = systemID;
        this.userSSID = userSSID;
        this.userName = userName;
        this.expireDate = expireDate;
        this.ofeigurVersion = ofeigurVersion;
        this.timestamp = timestamp;
    }
    
    public String getOfeigurVersion() {
        return ofeigurVersion;
    }
    
    public void setOfeigurVersion(String ofeigurVersion) {
        this.ofeigurVersion = ofeigurVersion;
    }
    
    public int getUserID() {
        return userID;
    }
    
    public void setUserID(int userID) {
        this.userID = userID;
    }
    
    public int getGroupID() {
        return groupID;
    }
    
    public void setGroupID(int groupID) {
        this.groupID = groupID;
    }
    
    public int getSystemID() {
        return systemID;
    }
    
    public void setSystemID(int systemID) {
        this.systemID = systemID;
    }
    
    public String getUserSSID() {
        return userSSID;
    }
    
    public void setUserSSID(String userSSID) {
        this.userSSID = userSSID;
    }

    public String getUserName() {
        return userName;
    }
    
    public void setUserName(String userName) {
        this.userName = userName;
    }    

    public String getTimestamp() {
        return timestamp;
    }
    
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
    
    public String getExpireDate() {
        return expireDate;
    }
    
    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }
    }