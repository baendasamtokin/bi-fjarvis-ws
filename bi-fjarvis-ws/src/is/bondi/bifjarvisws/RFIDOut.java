package is.bondi.bifjarvisws;

import java.io.Serializable;

public class RFIDOut implements Serializable {

    private Sheep sheepRearwards;
    private int farmID;
    
    public RFIDOut() {
    }
    
    public RFIDOut(int farmID, Sheep sheepRearwards)
    {
        this.farmID = farmID;
        this.sheepRearwards = sheepRearwards;
    }
    
    public Sheep getSheepRearwards() {
        return sheepRearwards;
    }
    
    public void setSheepRearwards(Sheep sheepRearwards) {
        this.sheepRearwards = sheepRearwards;
    }

    public int getFarmID() {
        return farmID;
    }
    
    public void setSheepRearwards(int farmID) {
        this.farmID = farmID;
    }
}