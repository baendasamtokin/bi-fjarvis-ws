package is.bondi.bifjarvisws;

import java.io.Serializable;

public class Farm implements Serializable {

    private int farmSeqNumber;      //bu_numer
    private int farmID;             //
    private String farmName;        //bu heiti
    private String sheepFarmID;     //b�jarn�mer
    private String holdingID;       //skh n�mer
    private int holdingTypeID;      //b�rekstur n�mer
    private String communeNumName;
    private String communeName;
    private String keeperID;        //umr��ama�ur kennitala
    private String keeperName;      //umr��ama�ur nafn
    private String keeperAddress;   //umr��ama�ur heimilisfang
    private String keeperZip;       //umr��ama�ur p�stn�mer
    private String keeperLocation;  //umr��ama�ur sta�ur
    private String keeperEmail;     //umr��ama�ur netfang
    private int groupSeqNumber;     //uppgjor_numer
    private int groupID;            //uppgjorsn�mer
    private String groupKeeperName; //uppgj�r - pers�na nafn
    private String groupFarmName;   //uppgj�r - bu heiti
    private int productionYear;     //framlei�slu�r
    private int countryCode;        //landsk��i
    
    public Farm() {
    }
    
    public Farm(int farmSeqNumber, int farmID, String farmName, String sheepFarmID, String holdingID,      
                int holdingTypeID, String communeNumName, String communeName, String keeperID, String keeperName,     
                String keeperAddress, String keeperZip, String keeperLocation, String keeperEmail, int groupSeqNumber,    
                int groupID, String groupKeeperName, String groupFarmName, int productionYear, int countryCode)       
    {
        this.farmSeqNumber =  farmSeqNumber;
        this.farmID = farmID;         
        this.farmName = farmName;
        this.sheepFarmID = sheepFarmID;
        this.holdingID = holdingID;
        this.holdingTypeID = holdingTypeID;
        this.communeNumName = communeNumName;
        this.communeName = communeName;
        this.keeperID = keeperID;
        this.keeperName = keeperName;
        this.keeperAddress = keeperAddress;
        this.keeperZip = keeperZip;
        this.keeperLocation = keeperLocation;
        this.keeperEmail = keeperEmail;
        this.groupSeqNumber = groupSeqNumber; 
        this.groupID = groupID;
        this.groupKeeperName = groupKeeperName;
        this.groupFarmName = groupFarmName;
        this.productionYear = productionYear;
        this.countryCode = countryCode;

    }
    
    public String getCommuneName() {
        return communeName;
    }
    
    public void setCommuneName(String communeName) {
        this.communeName = communeName;
    }
    
    public int getHoldingTypeID() {
        return holdingTypeID;
    }
    
    public void setHoldingTypeID(int holdingTypeID) {
        this.holdingTypeID = holdingTypeID;
    }
    
    public String getKeeperName() {
        return keeperName;
    }
    
    public void setKeeperName(String keeperName) {
        this.keeperName = keeperName;
    }
    
    public String getFarmName() {
        return farmName;
    }
    
    public void setFarmName(String farmName) {
        this.farmName = farmName;
    }
    
    public String getGroupFarmName() {
        return groupFarmName;
    }
    
    public void setGroupFarmName(String groupFarmName) {
        this.groupFarmName = groupFarmName;
    }
    
    public int getProductionYear() {
        return productionYear;
    }
    
    public void setProductionYear(int productionYear) {
        this.productionYear = productionYear;
    }
    
    public String getKeeperZip() {
        return keeperZip;
    }
    
    public void setKeeperZip(String keeperZip) {
        this.keeperZip = keeperZip;
    }
    
    public int getCountryCode() {
        return countryCode;
    }
    
    public void setCountryCode(int countryCode) {
        this.countryCode = countryCode;
    }
    
    public String getCommuneNumName() {
        return communeNumName;
    }
    
    public void setCommuneNumName(String communeNumName) {
        this.communeNumName = communeNumName;
    }
    
    public String getKeeperLocation() {
        return keeperLocation;
    }
    
    public void setKeeperLocation(String keeperLocation) {
        this.keeperLocation = keeperLocation;
    }
    
    public int getFarmSeqNumber() {
        return farmSeqNumber;
    }
    
    public void setFarmSeqNumber(int farmSeqNumber) {
        this.farmSeqNumber = farmSeqNumber;
    }
    
    public int getGroupID() {
        return groupID;
    }
    
    public void setGroupID(int groupID) {
        this.groupID = groupID;
    }
    
    public int getGroupSeqNumber() {
        return groupSeqNumber;
    }
    
    public void setGroupSeqNumber(int groupSeqNumber) {
        this.groupSeqNumber = groupSeqNumber;
    }
    
    public String getKeeperEmail() {
        return keeperEmail;
    }
    
    public void setKeeperEmail(String keeperEmail) {
        this.keeperEmail = keeperEmail;
    }
    
    public String getGroupKeeperName() {
        return groupKeeperName;
    }
    
    public void setGroupKeeperName(String groupKeeperName) {
        this.groupKeeperName = groupKeeperName;
    }
    
    public String getKeeperAddress() {
        return keeperAddress;
    }
    
    public void setKeeperAddress(String keeperAddress) {
        this.keeperAddress = keeperAddress;
    }
    
    public String getSheepFarmID() {
        return sheepFarmID;
    }
    
    public void setSheepFarmID(String sheepFarmID) {
        this.sheepFarmID = sheepFarmID;
    }
    
    public String getHoldingID() {
        return holdingID;
    }
    
    public void setHoldingID(String holdingID) {
        this.holdingID = holdingID;
    }
    
    public int getFarmID() {
        return farmID;
    }
    
    public void setFarmID(int farmID) {
        this.farmID = farmID;
    }
    
    public String getKeeperID() {
        return keeperID;
    }
    
    public void setKeeperID(String keeperID) {
        this.keeperID = keeperID;
    }
}
