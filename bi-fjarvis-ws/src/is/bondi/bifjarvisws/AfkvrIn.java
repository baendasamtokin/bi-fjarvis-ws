package is.bondi.bifjarvisws;

import java.io.Serializable;

public class AfkvrIn implements Serializable {

    private String dagsetning;// DD.MM.YYYY 
    private int taeki;// X
    private int domari;// X
    private int maelingaMadur;// X
    private int kyn;// X
    private String lifthungi;
    private int fotleggur;// XXX
    private String omvodvi;// XX
    private String omfita;// X
    private String logun;// v��va X
    private String haus;// XXX
    private String halsHerdar;// XXX
    private String bringaUtlogur;// XXX
    private String bak;// XXX
    private String malir;// XXX
    private String laeri;// XXX
    private String ull;// XXX
    private String faetur;// XXX
    private String samraemi;// XXX
    private String alls;// XXX
    private String athugasemdir;// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
       
    public AfkvrIn() {
    }

    public AfkvrIn(String dagsetning, int taeki,int domari,int maelingaMadur,                    
                    int kyn, String lifthungi, int fotleggur, String omvodvi, String omfita, String logun, String haus,
                    String halsHerdar, String bringaUtlogur, String bak, String malir,
                    String laeri, String ull, String faetur, String samraemi, String alls,
                    String athugasemdir)
    {
        this.dagsetning = dagsetning;
        this.taeki = taeki;
        this.domari = domari;
        this.maelingaMadur = maelingaMadur;        
        this.kyn = kyn;        
        this.lifthungi = lifthungi;
        this.fotleggur = fotleggur;
        this.omvodvi = omvodvi;
        this.omfita = omfita;
        this.logun = logun;
        this.haus = haus;
        this.halsHerdar = halsHerdar;
        this.bringaUtlogur = bringaUtlogur;
        this.bak = bak;
        this.malir = malir;
        this.laeri = laeri;
        this.ull = ull;
        this.faetur = faetur;
        this.samraemi = samraemi;
        this.alls = alls;
        this.athugasemdir = athugasemdir;
    }
  
    public void setDagsetning(String dagsetning)
    {
        this.dagsetning = dagsetning;
    }
  
    public String getDagsetning()
    {
        return dagsetning;
    }
  
    public void setTaeki(int taeki)
    {
        this.taeki = taeki;
    }
  
    public int getTaeki()
    {
        return taeki;
    }
  
    public void setDomari(int domari)
    {
        this.domari = domari;
    }
  
    public int getDomari()
    {
        return domari;
    }
  
    public void setMaelingaMadur(int maelingaMadur)
    {
        this.maelingaMadur = maelingaMadur;
    }
  
    public int getMaelingaMadur()
    {
        return maelingaMadur;
    }
  
    public void setKyn(int kyn)
    {
        this.kyn = kyn;
    }
  
    public int getKyn()
    {
        return kyn;
    }
  
    public void setFotleggur(int fotleggur)
    {
        this.fotleggur = fotleggur;
    }
  
    public int getFotleggur()
    {
        return fotleggur;
    }
  
    public void setOmvodvi(String omvodvi)
    {
        this.omvodvi = omvodvi;
    }
  
    public String getOmvodvi()
    {
        return omvodvi;
    }
  
    public void setOmfita(String omfita)
    {
        this.omfita = omfita;
    }
  
    public String getOmfita()
    {
        return omfita;
    }
  
    public void setLogun(String logun)
    {
        this.logun = logun;
    }
  
    public String getLogun()
    {
        return logun;
    }
  
    public void setHaus(String haus)
    {
        this.haus = haus;
    }
  
    public String getHaus()
    {
        return haus;
    }
  
    public void setHalsHerdar(String halsHerdar)
    {
        this.halsHerdar = halsHerdar;
    }
  
    public String getHalsHerdar()
    {
        return halsHerdar;
    }
  
    public void setBringaUtlogur(String bringaUtlogur)
    {
        this.bringaUtlogur = bringaUtlogur;
    }
  
    public String getBringaUtlogur()
    {
        return bringaUtlogur;
    }
  
    public void setBak(String bak)
    {
        this.bak = bak;
    }
  
    public String getBak()
    {
        return bak;
    }
  
    public void setMalir(String malir)
    {
        this.malir = malir;
    }
  
    public String getMalir()
    {
        return malir;
    }
  
    public void setLaeri(String laeri)
    {
        this.laeri = laeri;
    }
  
    public String getLaeri()
    {
        return laeri;
    }
  
    public void setUll(String ull)
    {
        this.ull = ull;
    }
  
    public String getUll()
    {
        return ull;
    }
  
    public void setFaetur(String faetur)
    {
        this.faetur = faetur;
    }
  
    public String getFaetur()
    {
        return faetur;
    }
  
    public void setSamraemi(String samraemi)
    {
        this.samraemi = samraemi;
    }
  
    public String getSamraemi()
    {
        return samraemi;
    }
  
    public void setAlls(String alls)
    {
        this.alls = alls;
    }
  
    public String getAlls()
    {
        return alls;
    }
  
    public void setAthugasemdir(String athugasemdir)
    {
        this.athugasemdir = athugasemdir;
    }
  
    public String getAthugasemdir()
    {
        return athugasemdir;
    }

  public void setLifthungi(String lifthungi)
  {
    this.lifthungi = lifthungi;
  }

  public String getLifthungi()
  {
    return lifthungi;
  }
}
