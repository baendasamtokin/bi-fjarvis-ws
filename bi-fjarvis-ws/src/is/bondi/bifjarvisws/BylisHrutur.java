package is.bondi.bifjarvisws;

import java.io.Serializable;

public class BylisHrutur implements Serializable {
    private int busNumer;
    private String Valnumer;
    private String Ormerki;
    private int hruturNumer;
    private String Nafn;
    private int Litur;
    private int blupFita;
    private int blupGerd;
    private int blup1Mjolkurlagni;
    private int blup1Frjosemi;
    private int blup2Mjolkurlagni;
    private int blup2Frjosemi;
    private int blup3Mjolkurlagni;
    private int blup3Frjosemi;
    private int blup4Mjolkurlagni;
    private int blup4Frjosemi;
    private int blupEinkunnM;
    private int blupEinkunnF;
    private String ValnumerFF;
    private String OrmerkiFF;
    private int NumerFF;
    private String NafnFF;
    private int LiturFF;
    private String ValnumerFFF;
    private String OrmerkiFFF;
    private int NumerFFF;
    private String NafnFFF;
    private int LiturFFF;
    private String ValnumerMM;
    private String OrmerkiMM;
    private int NumerMM;
    private String NafnMM;
    private int LiturMM;
    private String ValnumerMMM;
    private String OrmerkiMMM;
    private int NumerMMM;
    private String NafnMMM;
    private int LiturMMM;


    public BylisHrutur() {
    }

    public BylisHrutur(int busNumer, String Valnumer, String Ormerki, 
                       int hruturNumer, String Nafn, int Litur, int blupFita, 
                       int blupGerd, int blup1Mjolkurlagni, int blup1Frjosemi, 
                       int blup2Mjolkurlagni, int blup2Frjosemi, 
                       int blup3Mjolkurlagni, int blup3Frjosemi, 
                       int blup4Mjolkurlagni, int blup4Frjosemi, 
                       int blupEinkunnM, int blupEinkunnF, String ValnumerFF, 
                       String OrmerkiFF, int NumerFF, String NafnFF, 
                       int LiturFF, String ValnumerFFF, String OrmerkiFFF, 
                       int NumerFFF, String NafnFFF, int LiturFFF, 
                       String ValnumerMM, String OrmerkiMM, int NumerMM, 
                       String NafnMM, int LiturMM, String ValnumerMMM, 
                       String OrmerkiMMM, int NumerMMM, String NafnMMM, 
                       int LiturMMM) {
        this.busNumer = busNumer;
        this.Valnumer = Valnumer;
        this.Ormerki = Ormerki;
        this.hruturNumer = hruturNumer;
        this.Nafn = Nafn;
        this.Litur = Litur;
        this.blupFita = blupFita;
        this.blupGerd = blupGerd;
        this.blup1Mjolkurlagni = blup1Mjolkurlagni;
        this.blup1Frjosemi = blup1Frjosemi;
        this.blup2Mjolkurlagni = blup2Mjolkurlagni;
        this.blup2Frjosemi = blup2Frjosemi;
        this.blup3Mjolkurlagni = blup3Mjolkurlagni;
        this.blup3Frjosemi = blup3Frjosemi;
        this.blup4Mjolkurlagni = blup4Mjolkurlagni;
        this.blup4Frjosemi = blup4Frjosemi;
        this.blupEinkunnM = blupEinkunnM;
        this.blupEinkunnF = blupEinkunnF;
        this.ValnumerFF = ValnumerFF;
        this.OrmerkiFF = OrmerkiFF;
        this.NumerFF = NumerFF;
        this.NafnFF = NafnFF;
        this.LiturFF = LiturFF;
        this.ValnumerFFF = ValnumerFFF;
        this.OrmerkiFFF = OrmerkiFFF;
        this.NumerFFF = NumerFFF;
        this.NafnFFF = NafnFFF;
        this.LiturFFF = LiturFFF;
        this.ValnumerMM = ValnumerMM;
        this.OrmerkiMM = OrmerkiMM;
        this.NumerMM = NumerMM;
        this.NafnMM = NafnMM;
        this.LiturMM = LiturMM;
        this.ValnumerMMM = ValnumerMMM;
        this.OrmerkiMMM = OrmerkiMMM;
        this.NumerMMM = NumerMMM;
        this.NafnMMM = NafnMMM;
        this.LiturMMM = LiturMMM;

    }


    public void setBusNumer(int busNumer) {
        this.busNumer = busNumer;
    }

    public int getBusNumer() {
        return busNumer;
    }


    public void setValnumer(String valnumer) {
        this.Valnumer = valnumer;
    }

    public String getValnumer() {
        return Valnumer;
    }

    public void setOrmerki(String ormerki) {
        this.Ormerki = ormerki;
    }

    public String getOrmerki() {
        return Ormerki;
    }

    public void setHruturNumer(int hruturNumer) {
        this.hruturNumer = hruturNumer;
    }

    public int getHruturNumer() {
        return hruturNumer;
    }

    public void setNafn(String nafn) {
        this.Nafn = nafn;
    }

    public String getNafn() {
        return Nafn;
    }

    public void setLitur(int litur) {
        this.Litur = litur;
    }

    public int getLitur() {
        return Litur;
    }

    public void setBlupFita(int blupFita) {
        this.blupFita = blupFita;
    }

    public int getBlupFita() {
        return blupFita;
    }

    public void setBlupGerd(int blupGerd) {
        this.blupGerd = blupGerd;
    }

    public int getBlupGerd() {
        return blupGerd;
    }

    public void setBlup1Mjolkurlagni(int blup1Mjolkurlagni) {
        this.blup1Mjolkurlagni = blup1Mjolkurlagni;
    }

    public int getBlup1Mjolkurlagni() {
        return blup1Mjolkurlagni;
    }

    public void setBlup1Frjosemi(int blup1Frjosemi) {
        this.blup1Frjosemi = blup1Frjosemi;
    }

    public int getBlup1Frjosemi() {
        return blup1Frjosemi;
    }

    public void setBlup2Mjolkurlagni(int blup2Mjolkurlagni) {
        this.blup2Mjolkurlagni = blup2Mjolkurlagni;
    }

    public int getBlup2Mjolkurlagni() {
        return blup2Mjolkurlagni;
    }

    public void setBlup2Frjosemi(int blup2Frjosemi) {
        this.blup2Frjosemi = blup2Frjosemi;
    }

    public int getBlup2Frjosemi() {
        return blup2Frjosemi;
    }

    public void setBlup3Mjolkurlagni(int blup3Mjolkurlagni) {
        this.blup3Mjolkurlagni = blup3Mjolkurlagni;
    }

    public int getBlup3Mjolkurlagni() {
        return blup3Mjolkurlagni;
    }

    public void setBlup3Frjosemi(int blup3Frjosemi) {
        this.blup3Frjosemi = blup3Frjosemi;
    }

    public int getBlup3Frjosemi() {
        return blup3Frjosemi;
    }

    public void setBlup4Mjolkurlagni(int blup4Mjolkurlagni) {
        this.blup4Mjolkurlagni = blup4Mjolkurlagni;
    }

    public int getBlup4Mjolkurlagni() {
        return blup4Mjolkurlagni;
    }

    public void setBlup4Frjosemi(int blup4Frjosemi) {
        this.blup4Frjosemi = blup4Frjosemi;
    }

    public int getBlup4Frjosemi() {
        return blup4Frjosemi;
    }

    public void setBlupEinkunnM(int blupEinkunnM) {
        this.blupEinkunnM = blupEinkunnM;
    }

    public int getBlupEinkunnM() {
        return blupEinkunnM;
    }

    public void setBlupEinkunnF(int blupEinkunnF) {
        this.blupEinkunnF = blupEinkunnF;
    }

    public int getBlupEinkunnF() {
        return blupEinkunnF;
    }

    public void setValnumerFF(String ValnumerFF) {
        this.ValnumerFF = ValnumerFF;
    }

    public String getValnumerFF() {
        return ValnumerFF;
    }

    public void setOrmerkiFF(String OrmerkiFF) {
        this.OrmerkiFF = OrmerkiFF;
    }

    public String getOrmerkiFF() {
        return OrmerkiFF;
    }

    public void setNumerFF(int NumerFF) {
        this.NumerFF = NumerFF;
    }

    public int getNumerFF() {
        return NumerFF;
    }

    public void setNafnFF(String NafnFF) {
        this.NafnFF = NafnFF;
    }

    public String getNafnFF() {
        return NafnFF;
    }

    public void setLiturFF(int LiturFF) {
        this.LiturFF = LiturFF;
    }

    public int getLiturFF() {
        return LiturFF;
    }

    public void setValnumerFFF(String ValnumerFFF) {
        this.ValnumerFFF = ValnumerFFF;
    }

    public String getValnumerFFF() {
        return ValnumerFFF;
    }

    public void setOrmerkiFFF(String OrmerkiFFF) {
        this.OrmerkiFFF = OrmerkiFFF;
    }

    public String getOrmerkiFFF() {
        return OrmerkiFFF;
    }

    public void setNumerFFF(int NumerFFF) {
        this.NumerFFF = NumerFFF;
    }

    public int getNumerFFF() {
        return NumerFFF;
    }

    public void setNafnFFF(String NafnFFF) {
        this.NafnFFF = NafnFFF;
    }

    public String getNafnFFF() {
        return NafnFFF;
    }

    public void setLiturFFF(int LiturFFF) {
        this.LiturFFF = LiturFFF;
    }

    public int getLiturFFF() {
        return LiturFFF;
    }

    public void setValnumerMM(String ValnumerMM) {
        this.ValnumerMM = ValnumerMM;
    }

    public String getValnumerMM() {
        return ValnumerMM;
    }

    public void setOrmerkiMM(String OrmerkiMM) {
        this.OrmerkiMM = OrmerkiMM;
    }

    public String getOrmerkiMM() {
        return OrmerkiMM;
    }

    public void setMmNumer(int NumerMM) {
        this.NumerMM = NumerMM;
    }

    public int getNumerMM() {
        return NumerMM;
    }

    public void setNafnMM(String NafnMM) {
        this.NafnMM = NafnMM;
    }

    public String getNafnMM() {
        return NafnMM;
    }

    public void setLiturMM(int LiturMM) {
        this.LiturMM = LiturMM;
    }

    public int getLiturMM() {
        return LiturMM;
    }

    public void setValnumerMMM(String ValnumerMMM) {
        this.ValnumerMMM = ValnumerMMM;
    }

    public String getValnumerMMM() {
        return ValnumerMMM;
    }

    public void setOrmerkiMMM(String OrmerkiMMM) {
        this.OrmerkiMMM = OrmerkiMMM;
    }

    public String getOrmerkiMMM() {
        return OrmerkiMMM;
    }

    public void setNumerMMM(int NumerMMM) {
        this.NumerMMM = NumerMMM;
    }

    public int getNumerMMM() {
        return NumerMMM;
    }

    public void setNafnMMM(String NafnMMM) {
        this.NafnMMM = NafnMMM;
    }

    public String getNafnMMM() {
        return NafnMMM;
    }

    public void setLiturMMM(int LiturMMM) {
        this.LiturMMM = LiturMMM;
    }

    public int getLiturMMM() {
        return LiturMMM;
    }
}
