package is.bondi.bifjarvisws;

import java.io.Serializable;


public class AnimalIds  implements Serializable {

    private int einstaklingurNumer;     // frumlykill einstaklings
    private int kyn;                    // kyn einstaklings (1=kk / 2=kvk)
    private String valnr;               // valn�mer einstaklings
    private String ormerki;             // �rmerki � skr�
    private int tegund;                 // tegund d�rs

    public AnimalIds( ) {
    }

    public AnimalIds( int einstaklingurNumer, int kyn, String valnr, String ormerki, int tegund) 
    {
    this.einstaklingurNumer = einstaklingurNumer;
    this.kyn = kyn;
    this.valnr = valnr;
    this.ormerki = ormerki;
    this.tegund = tegund;
    }    
    
    public int getEinstaklingurNumer() {
        return einstaklingurNumer;
    }
    
    public void setEinstaklingurNumer(int einstaklingurNumer) {
        this.einstaklingurNumer = einstaklingurNumer;
    }    
    
    public int getKyn() {
        return kyn;
    }
    
    public void setKyn(int kyn) {
        this.kyn = kyn;
    }     
    
    public String getValnr() {
        return valnr;
    }
    
    public void setValnr(String valnr) {
        this.valnr = valnr;
    } 
    
    public String getOrmerki() {
        return ormerki;
    }
    
    public void setOrmerki(String ormerki) {
        this.ormerki = ormerki;
    }   
    
    public int getTegund() {
        return tegund;
    }
    
    public void setTegund(int tegund) {
        this.tegund = tegund;
    }     
    
    
}
