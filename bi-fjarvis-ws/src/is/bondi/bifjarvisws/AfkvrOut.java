package is.bondi.bifjarvisws;

import java.io.Serializable;

public class AfkvrOut implements Serializable {

    //private Sheep sheepRearwards;
    //private int farmID;
      
    private String dagsetning;// DD.MM.YYYY 
    private int taeki;// X
    private int domari;// X
    private int maelingaMadur;// X
    private String sjonraentNumerLambs;// XXXX
    private String rfid;// XXX XXXXXXXXXXXX
    private String faedingardagur;// DD.MM.YYYY
    private int kyn;// X
    private int litur;// XX
    private int burdur;// X
    private int gengin;// X
    private int fotleggur;// XXX
    private String omvodvi;// XX
    private String omfita;// X
    private String logun;// v��va X
    private String haus;// XXX
    private String halsHerdar;// XXX
    private String bringaUtlogur;// XXX
    private String bak;// XXX
    private String malir;// XXX
    private String laeri;// XXX
    private String ull;// XXX
    private String faetur;// XXX
    private String samraemi;// XXX
    private String alls;// XXX
    private String athugasemdir;// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    private String sjonraentNumerFodur;// XXXXX
    private String nafnFodur;
    private String sjonraentNumerModur;// XXXXX
    private String nafnModur;
    private String blup_fita;
    private String blup_gerd;
    private String blup_mjolk;
    private String blup_frjosemi; 
    private Thungi [] thungi;
    
       
    public AfkvrOut() {
    }
    
    public AfkvrOut(String dagsetning, int taeki,int domari,int maelingaMadur,
                    String sjonraentNumerLambs, String rfid, String faedingardagur,
                    int kyn, int litur, int burdur, int gengin, int fotleggur,
                    String omvodvi, String omfita, String logun, String haus,
                    String halsHerdar, String bringaUtlogur, String bak, String malir,
                    String laeri, String ull, String faetur, String samraemi, String alls,
                    String athugasemdir, String sjonraentNumerFodur, String nafnFodur,
                    String sjonraentNumerModur, String nafnModur, String blup_fita,
                    String blup_gerd, String blup_mjolk, String blup_frjosemi, 
                    Thungi [] thungi)
    {
        this.dagsetning = dagsetning;
        this.taeki = taeki;
        this.domari = domari;
        this.maelingaMadur = maelingaMadur;
        this.sjonraentNumerLambs = sjonraentNumerLambs;
        this.rfid = rfid;
        this.faedingardagur = faedingardagur;
        this.kyn = kyn;
        this.litur = litur;
        this.burdur = burdur;
        this.gengin = gengin;
        this.fotleggur = fotleggur;
        this.omvodvi = omvodvi;
        this.omfita = omfita;
        this.logun = logun;
        this.haus = haus;
        this.halsHerdar = halsHerdar;
        this.bringaUtlogur = bringaUtlogur;
        this.bak = bak;
        this.malir = malir;
        this.laeri = laeri;
        this.ull = ull;
        this.faetur = faetur;
        this.samraemi = samraemi;
        this.alls = alls;
        this.athugasemdir = athugasemdir;
        this.sjonraentNumerFodur = sjonraentNumerFodur;
        this.nafnFodur = nafnFodur;
        this.sjonraentNumerModur = sjonraentNumerModur;
        this.nafnModur = nafnModur;
        this.blup_fita = blup_fita;
        this.blup_gerd = blup_gerd;
        this.blup_mjolk = blup_mjolk;
        this.blup_frjosemi = blup_frjosemi; 
        this.thungi = thungi;
    //    this.farmID = farmID;
    //    this.sheepRearwards = sheepRearwards;
    }
    
/*    public Sheep getSheepRearwards() {
        return sheepRearwards;
    }
    
    public void setSheepRearwards(Sheep sheepRearwards) {
        this.sheepRearwards = sheepRearwards;
    }

    public int getFarmID() {
        return farmID;
    }
    
    public void setSheepRearwards(int farmID) {
        this.farmID = farmID;
    }

  public void setFarmID(int farmID)
  {
    this.farmID = farmID;
  }
*/
  public void setTaeki(int taeki)
  {
    this.taeki = taeki;
  }

  public int getTaeki()
  {
    return taeki;
  }

  public void setDomari(int domari)
  {
    this.domari = domari;
  }

  public int getDomari()
  {
    return domari;
  }

  public void setMaelingaMadur(int maelingaMadur)
  {
    this.maelingaMadur = maelingaMadur;
  }

  public int getMaelingaMadur()
  {
    return maelingaMadur;
  }

  public void setSjonraentNumerLambs(String sjonraentNumerLambs)
  {
    this.sjonraentNumerLambs = sjonraentNumerLambs;
  }

  public String getSjonraentNumerLambs()
  {
    return sjonraentNumerLambs;
  }

  public void setRfid(String rfid)
  {
    this.rfid = rfid;
  }

  public String getRfid()
  {
    return rfid;
  }

  public void setFaedingardagur(String faedingardagur)
  {
    this.faedingardagur = faedingardagur;
  }

  public String getFaedingardagur()
  {
    return faedingardagur;
  }

  public void setKyn(int kyn)
  {
    this.kyn = kyn;
  }

  public int getKyn()
  {
    return kyn;
  }

  public void setLitur(int litur)
  {
    this.litur = litur;
  }

  public int getLitur()
  {
    return litur;
  }

  public void setBurdur(int burdur)
  {
    this.burdur = burdur;
  }

  public int getBurdur()
  {
    return burdur;
  }

  public void setGengin(int gengin)
  {
    this.gengin = gengin;
  }

  public int getGengin()
  {
    return gengin;
  }

  public void setFotleggur(int fotleggur)
  {
    this.fotleggur = fotleggur;
  }

  public int getFotleggur()
  {
    return fotleggur;
  }

  public void setOmvodvi(String omvodvi)
  {
    this.omvodvi = omvodvi;
  }

  public String getOmvodvi()
  {
    return omvodvi;
  }

  public void setOmfita(String omfita)
  {
    this.omfita = omfita;
  }

  public String getOmfita()
  {
    return omfita;
  }

  public void setLogun(String logun)
  {
    this.logun = logun;
  }

  public String getLogun()
  {
    return logun;
  }

  public void setHaus(String haus)
  {
    this.haus = haus;
  }

  public String getHaus()
  {
    return haus;
  }

  public void setHalsHerdar(String halsHerdar)
  {
    this.halsHerdar = halsHerdar;
  }

  public String getHalsHerdar()
  {
    return halsHerdar;
  }

  public void setBringaUtlogur(String bringaUtlogur)
  {
    this.bringaUtlogur = bringaUtlogur;
  }

  public String getBringaUtlogur()
  {
    return bringaUtlogur;
  }

  public void setBak(String bak)
  {
    this.bak = bak;
  }

  public String getBak()
  {
    return bak;
  }

  public void setMalir(String malir)
  {
    this.malir = malir;
  }

  public String getMalir()
  {
    return malir;
  }

  public void setLaeri(String laeri)
  {
    this.laeri = laeri;
  }

  public String getLaeri()
  {
    return laeri;
  }

  public void setUll(String ull)
  {
    this.ull = ull;
  }

  public String getUll()
  {
    return ull;
  }

  public void setFaetur(String faetur)
  {
    this.faetur = faetur;
  }

  public String getFaetur()
  {
    return faetur;
  }

  public void setSamraemi(String samraemi)
  {
    this.samraemi = samraemi;
  }

  public String getSamraemi()
  {
    return samraemi;
  }

  public void setAlls(String alls)
  {
    this.alls = alls;
  }

  public String getAlls()
  {
    return alls;
  }

  public void setAthugasemdir(String athugasemdir)
  {
    this.athugasemdir = athugasemdir;
  }

  public String getAthugasemdir()
  {
    return athugasemdir;
  }

  public void setSjonraentNumerFodur(String sjonraentNumerFodur)
  {
    this.sjonraentNumerFodur = sjonraentNumerFodur;
  }

  public String getSjonraentNumerFodur()
  {
    return sjonraentNumerFodur;
  }

  public void setNafnFodur(String nafnFodur)
  {
    this.nafnFodur = nafnFodur;
  }

  public String getNafnFodur()
  {
    return nafnFodur;
  }

  public void setSjonraentNumerModur(String sjonraentNumerModur)
  {
    this.sjonraentNumerModur = sjonraentNumerModur;
  }

  public String getSjonraentNumerModur()
  {
    return sjonraentNumerModur;
  }

  public void setNafnModur(String nafnModur)
  {
    this.nafnModur = nafnModur;
  }

  public String getNafnModur()
  {
    return nafnModur;
  }

  public void setDagsetning(String dagsetning)
  {
    this.dagsetning = dagsetning;
  }

  public String getDagsetning()
  {
    return dagsetning;
  }

  public void setThungi(Thungi[] thungi)
  {
    this.thungi = thungi;
  }

  public Thungi[] getThungi()
  {
    return thungi;
  }

    public void setBlup_fita(String blup_fita) {
        this.blup_fita = blup_fita;
    }

    public String getBlup_fita() {
        return blup_fita;
    }

    public void setBlup_gerd(String blup_gerd) {
        this.blup_gerd = blup_gerd;
    }

    public String getBlup_gerd() {
        return blup_gerd;
    }

    public void setBlup_mjolk(String blup_mjolk) {
        this.blup_mjolk = blup_mjolk;
    }

    public String getBlup_mjolk() {
        return blup_mjolk;
    }

    public void setBlup_frjosemi(String blup_frjosemi) {
        this.blup_frjosemi = blup_frjosemi;
    }

    public String getBlup_frjosemi() {
        return blup_frjosemi;
    }
}
