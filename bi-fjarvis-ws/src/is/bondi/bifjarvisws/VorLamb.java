package is.bondi.bifjarvisws;

public class VorLamb 
{
    private int lambNumer;
    private String lambanumer;
    private String ormerki;
    private int kyn;
    private int litanumer;
    private String faedingarThungi;
    private int fjoldiGekk;
    private int fosturmodirNumer;
    private int afdrifVor;
    
    public VorLamb() 
    {
    }
    
    public VorLamb ( int lambNumer, String lambanumer, String ormerki, int kyn, int litanumer,
                   String faedingarThungi, int fjoldiGekk, int fosturmodirNumer, int afdrifVor)
    {
        this.lambNumer = lambNumer;
        this.lambanumer = lambanumer;
        this.ormerki = ormerki;
        this.kyn = kyn;
        this.litanumer = litanumer;
        this.faedingarThungi = faedingarThungi;
        this.fjoldiGekk = fjoldiGekk;
        this.fosturmodirNumer = fosturmodirNumer;
        this.afdrifVor = afdrifVor;
    }


    public void setLambNumer(int lambNumer) {
        this.lambNumer = lambNumer;
    }

    public int getLambNumer() {
        return lambNumer;
    }

    public void setLambanumer(String lambanumer) {
        this.lambanumer = lambanumer;
    }

    public String getLambanumer() {
        return lambanumer;
    }

    public void setOrmerki(String ormerki) {
        this.ormerki = ormerki;
    }

    public String getOrmerki() {
        return ormerki;
    }

    public void setKyn(int kyn) {
        this.kyn = kyn;
    }

    public int getKyn() {
        return kyn;
    }

    public void setLitanumer(int litanumer) {
        this.litanumer = litanumer;
    }

    public int getLitanumer() {
        return litanumer;
    }

    public void setFaedingarThungi(String faedingarThungi) {
        this.faedingarThungi = faedingarThungi;
    }

    public String getFaedingarThungi() {
        return faedingarThungi;
    }

    public void setFjoldiGekk(int fjoldiGekk) {
        this.fjoldiGekk = fjoldiGekk;
    }

    public int getFjoldiGekk() {
        return fjoldiGekk;
    }

    public void setFosturmodirNumer(int fosturmodirNumer) {
        this.fosturmodirNumer = fosturmodirNumer;
    }

    public int getFosturmodirNumer() {
        return fosturmodirNumer;
    }

    public void setAfdrifVor(int afdrifVor) {
        this.afdrifVor = afdrifVor;
    }

    public int getAfdrifVor() {
        return afdrifVor;
    }
}
