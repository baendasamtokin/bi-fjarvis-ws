package is.bondi.bifjarvisws;

import java.io.Serializable;

public class Domari implements Serializable{

    private int UniqSeqId;
    private String UniqIsId;
    private String Name;
    private String Email;
    private String Gsm;

    public Domari() {
    }
    
    public Domari(int uniqSeqId, String uniqIsId, String name, String email, String gsm ) {
        this.UniqSeqId = uniqSeqId;
        this.UniqIsId = uniqIsId;
        this.Name = name;
        this.Email = email;
        this.Gsm = gsm;
    }

  public void setUniqSeqId(int uniqSeqId)
  {
    this.UniqSeqId = uniqSeqId;
  }

  public int getUniqSeqId()
  {
    return UniqSeqId;
  }

  public void setUniqIsId(String uniqIsId)
  {
    this.UniqIsId = uniqIsId;
  }

  public String getUniqIsId()
  {
    return UniqIsId;
  }

  public void setName(String name)
  {
    this.Name = name;
  }

  public String getName()
  {
    return Name;
  }

  public void setEmail(String email)
  {
    this.Email = email;
  }

  public String getEmail()
  {
    return Email;
  }

  public void setGsm(String gsm)
  {
    this.Gsm = gsm;
  }

  public String getGsm()
  {
    return Gsm;
  }
}
