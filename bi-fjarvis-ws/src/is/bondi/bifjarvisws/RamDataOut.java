package is.bondi.bifjarvisws;

import java.io.Serializable;

public class RamDataOut implements Serializable {

    private int Busnumer;
    private BylisHrutur Hrutur;

    public RamDataOut() {
    }

    public RamDataOut(int Busnumer, BylisHrutur Hrutur) {
        this.Busnumer = Busnumer;
        this.Hrutur = Hrutur;
    }

    public int getBusnumer() {
        return Busnumer;
    }

    public void setBusnumer(int Busnumer) {
        this.Busnumer = Busnumer;
    }

    public BylisHrutur getHrutur() {
        return Hrutur;
    }

    public void setHrutur(BylisHrutur Hrutur) {
        this.Hrutur = Hrutur;
    }


}
