package is.bondi.bifjarvisws;

import java.io.Serializable;

public class SpringData implements Serializable {

    private String modirFRID;
    private String modirRFID;
    private String modirUniqId;
    private String modirNafn;
    private String modirBeitarsvaediNr;
    private String fjoldiLamba;
    private String burdardags;
    private String fadirFRID;
    private String fadirRFID;
    private String fadirNafn;
    private String fadirUniqId;
    private String fangdags;
    private String afdrifFangsNumer;
    private String modirAthsNumer;
    //lamb1
    private String lambFRID;
    private String lambRFID;
    private String lambUniqId;
    private String kyn;
    private String liturNumer;
    private String fosturmodirFRID;
    private String fosturmodirUniqId;
    private String fosturmodirRFID;
    private String faedingarthungi;
    private String afdrifVorNumer;
    private String afdrifVorFasti;

    public SpringData() {
    }
    
    public SpringData(String modirFRID, String modirRFID, String modirUniqId, 
                String modirNafn, String modirBeitarSvaediNr, String fjoldiLamba, String burdardags, String fadirFRID,   
                String fadirRFID, String fadirUniqId , String fadirNafn, String fangdags,
                String afdrifFangsNumer, String modirAthsNumer, 
                String lambFRID, String lambRFID, String lambUniqId, String kyn, String liturNumer, String fosturmodirFRID,
                String fosturmodirUniqId, String fosturmodirRFID, String faedingarthungi, String afdrifVorNumer, 
                String afdrifVorFasti
                )      
    {
        this.modirFRID = modirFRID;
        this.modirRFID = modirRFID;
        this.modirUniqId = modirUniqId;
        this.modirNafn = modirNafn;
        this.modirBeitarsvaediNr = modirBeitarSvaediNr;
        this.fjoldiLamba = fjoldiLamba;
        this.burdardags = burdardags;
        this.fadirFRID = fadirFRID;
        this.fadirRFID = fadirRFID;
        this.fadirUniqId = fadirUniqId;
        this.fadirNafn = fadirNafn;
        this.fangdags = fangdags;
        this.afdrifFangsNumer = afdrifFangsNumer;
        this.modirAthsNumer = modirAthsNumer;
        
        this.lambFRID = lambFRID;
        this.lambRFID = lambRFID;
        this.lambUniqId = lambUniqId;
        this.kyn = kyn;
        this.liturNumer = liturNumer;
        this.fosturmodirFRID = fosturmodirFRID;
        this.fosturmodirUniqId = fosturmodirUniqId;
        this.fosturmodirRFID = fosturmodirRFID;
        this.faedingarthungi = faedingarthungi;
        this.afdrifVorNumer = afdrifVorNumer;
        this.afdrifVorFasti = afdrifVorFasti;
     }


    public void setModirFRID(String modirFRID) {
        this.modirFRID = modirFRID;
    }

    public String getModirFRID() {
        return modirFRID;
    }

    public void setModirRFID(String modirRFID) {
        this.modirRFID = modirRFID;
    }

    public String getModirRFID() {
        return modirRFID;
    }

    public void setModirUniqId(String modirUniqId) {
        this.modirUniqId = modirUniqId;
    }

    public String getModirUniqId() {
        return modirUniqId;
    }

    public void setModirNafn(String modirNafn) {
        this.modirNafn = modirNafn;
    }

    public String getModirNafn() {
        return modirNafn;
    }

    public void setModirBeitarsvaediNr(String modirBeitarsvaediNr) {
        this.modirBeitarsvaediNr = modirBeitarsvaediNr;
    }

    public String getModirBeitarsvaediNr() {
        return modirBeitarsvaediNr;
    }

    public void setFjoldiLamba(String fjoldiLamba) {
        this.fjoldiLamba = fjoldiLamba;
    }

    public String getFjoldiLamba() {
        return fjoldiLamba;
    }

    public void setBurdardags(String burdardags) {
        this.burdardags = burdardags;
    }

    public String getBurdardags() {
        return burdardags;
    }

    public void setFadirFRID(String fadirFRID) {
        this.fadirFRID = fadirFRID;
    }

    public String getFadirFRID() {
        return fadirFRID;
    }

    public void setFadirRFID(String fadirRFID) {
        this.fadirRFID = fadirRFID;
    }

    public String getFadirRFID() {
        return fadirRFID;
    }

    public void setFadirNafn(String fadirNafn) {
        this.fadirNafn = fadirNafn;
    }

    public String getFadirNafn() {
        return fadirNafn;
    }

    public void setFadirUniqId(String fadirUniqId) {
        this.fadirUniqId = fadirUniqId;
    }

    public String getFadirUniqId() {
        return fadirUniqId;
    }

    public void setFangdags(String fangdags) {
        this.fangdags = fangdags;
    }

    public String getFangdags() {
        return fangdags;
    }

    public void setAfdrifFangsNumer(String afdrifFangsNumer) {
        this.afdrifFangsNumer = afdrifFangsNumer;
    }

    public String getAfdrifFangsNumer() {
        return afdrifFangsNumer;
    }

    public void setModirAthsNumer(String modirAthsNumer) {
        this.modirAthsNumer = modirAthsNumer;
    }

    public String getModirAthsNumer() {
        return modirAthsNumer;
    }

    public void setLambFRID(String lambFRID) {
        this.lambFRID = lambFRID;
    }

    public String getLambFRID() {
        return lambFRID;
    }

    public void setLambRFID(String lambRFID) {
        this.lambRFID = lambRFID;
    }

    public String getLambRFID() {
        return lambRFID;
    }

    public void setLambUniqId(String lambUniqId) {
        this.lambUniqId = lambUniqId;
    }

    public String getLambUniqId() {
        return lambUniqId;
    }

    public void setKyn(String kyn) {
        this.kyn = kyn;
    }

    public String getKyn() {
        return kyn;
    }

    public void setLiturNumer(String liturNumer) {
        this.liturNumer = liturNumer;
    }

    public String getLiturNumer() {
        return liturNumer;
    }

    public void setFosturmodirFRID(String fosturmodirFRID) {
        this.fosturmodirFRID = fosturmodirFRID;
    }

    public String getFosturmodirFRID() {
        return fosturmodirFRID;
    }

    public void setFaedingarthungi(String faedingarthungi) {
        this.faedingarthungi = faedingarthungi;
    }

    public String getFaedingarthungi() {
        return faedingarthungi;
    }

    public void setAfdrifVorNumer(String afdrifVorNumer) {
        this.afdrifVorNumer = afdrifVorNumer;
    }

    public String getAfdrifVorNumer() {
        return afdrifVorNumer;
    }

    public void setAfdrifVorFasti(String afdrifVorFasti) {
        this.afdrifVorFasti = afdrifVorFasti;
    }

    public String getAfdrifVorFasti() {
        return afdrifVorFasti;
    }

    public void setFosturmodirUniqId(String fosturmodirUniqId) {
        this.fosturmodirUniqId = fosturmodirUniqId;
    }

    public String getFosturmodirUniqId() {
        return fosturmodirUniqId;
    }

    public void setFosturmodirRFID(String fosturmodirRFID) {
        this.fosturmodirRFID = fosturmodirRFID;
    }

    public String getFosturmodirRFID() {
        return fosturmodirRFID;
    }
}
