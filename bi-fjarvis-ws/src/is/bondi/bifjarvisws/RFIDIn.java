package is.bondi.bifjarvisws;

import java.io.Serializable;

public class RFIDIn implements Serializable {

    private String warning;
    private int result;
       
   public RFIDIn() {
   }
   
   public String getWarning() {
       return warning;
   }
   
   public void setWarning(String warning) {
       this.warning = warning;
   }
   
   public int getResult() {
       return result;
   }
   
   public void setResult(int result) {
       this.result = result;
   }
}
